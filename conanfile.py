from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout


class PhiConan(ConanFile):
    name = "phi"
    version = "0.1"
    license = "GNU GPL v3"
    url = "https://gitlab.com/JavierJF/phi"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "tests": [True, False]}
    generators = "CMakeDeps"

    default_options = {
        "shared": False,
        "tests": True,
        "gtest/*:shared": False,
        "gtest/*:no_gmock": True,
        "gtest/*:no_main": True
    }

    def requirements(self):
        self.test_requires("gtest/1.11.0")
        self.requires("uerrno/0.1")
        self.requires("stdwrapper/0.1")

    def layout(self):
        cmake_layout(self)

    def source(self):
        self.run("git clone https://gitlab.com/JavierJF/Phi Phi")
        self.run("cd Phi && git checkout develop")

    def generate(self):
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["Phi"]
