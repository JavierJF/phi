#include <Phi/Sync/SVar.h>

#include <Std/Queue.h>
#include <Std/Utility.h>
#include <Std/Vector.h>

#include <uerrno/uerrno.h>

#include <gtest/gtest.h>
#include <chrono>
#include <thread>

#include <Phi/PTypes.h>

using namespace Phi;

TEST(SVar, SimpleTakePutCommunication) {
    SVar<int> sVar { 0 };

    std::thread get(
        [&sVar]() {
            auto newVal = takeSVar(sVar);
            ASSERT_EQ(newVal, 5);
        }
    );

    std::thread put(
        [&sVar]() {
            putSVar(sVar, 5);
        }
    );

    put.join();
    get.join();

    ASSERT_TRUE(true);
}

TEST(SVar, TakeSVarValueAndModify) {
    SVar<int> sVar { 0 };

    std::thread get(
        [&sVar]() {
            int newVal = takeSVar(sVar,
                [](int& val) -> int {
                     return val + 1;
                }
            );
            ASSERT_EQ(newVal, 6);
        }
    );

    std::thread put(
        [&sVar]() {
            putSVar(sVar, 5);
        }
    );

    put.join();
    get.join();
}

TEST(SVar, takeAndInitSVar) {
    SVar<int> s {0};
    putSVar(s, 0);
    takeAndInitSVar(s, [](int& val) -> void { val = 1; });
    ASSERT_EQ(takeSVar(s), 1);
}

TEST(SVar, UpdSVarVoid) {
    SVar<int> s {0};
    updSVar(s, [](auto& i) { i = 1;} );
    ASSERT_EQ(takeSVar(s), 1);
}

TEST(SVar, UpdSVarVoidOptParam) {
    SVar<int> s {0};
    updSVar(s, [](auto& i, auto&& b) { i = 1 + b; }, 1);
    ASSERT_EQ(takeSVar(s), 2);
}

enum class Result : PUInt32 {
    OK = 0,
    Computed = 1
};

TEST(SVar, UpdSVarEither) {
    SVar<int> s {0};

    Fn<Either<Result,errno_t>(int&)> increment{
        [](int& v) -> Either<Result, errno_t> {
            return Left<Result, errno_t>( Result( static_cast<PUInt32>( v ) + 1) );
        }
    };

    auto res = updSVar(s, increment);

    pMatch(res,
        [](auto value) {
            ASSERT_EQ(value, Result::Computed);
        },
        [](auto) {
            ASSERT_FALSE(true);
        }
    );
}

TEST(SVar, TakePutSVarAsChannelToReturn) {
    using namespace std::chrono_literals;

    SVar<Result> toWorker { Result::OK };
    SVar<Result> toMain { Result::OK };

    std::thread worker(
        [&toMain,&toWorker]() {
            auto newVal =
                takePutSVar(
                    toWorker,
                    toMain,
                    fnerr_t(E_FN_SUCCESS),
                    Fn<Either<Result,fnerr_t>(Result&)>(
                        [](auto& val) -> Either<Result,fnerr_t> {
                            return Left<Result,fnerr_t>(
                                Result( static_cast<PUInt32>( val ) + 1)
                            );
                        }
                    )
                );

            ASSERT_EQ(newVal, E_FN_SUCCESS);
        }
    );

    std::thread main(
        [&toWorker, &toMain]() {
            putSVar(toWorker, Result( 5 ) );

            auto res = takeSVar(toMain);

            ASSERT_EQ(res, Result(6) );
        }
    );

    worker.join();
    main.join();
}
