#include <Phi/Sync/WMutex.h>
#include <Phi/Sync/Lock.h>

#include <thread>
#include <utility>
#include <gtest/gtest.h>

TEST(WMutex, MutexConstruction) {
    WMutex<int> m {0};
}

TEST(WMutex, MutexLocking) {
    WMutex<int> m {0};

    {
        Lock<int> _lock(m);
        (*_lock) = 2;
    }

}

TEST(WMutex, MutexWaiting) {
    WMutex<Pair<int,bool>>  m {{0,0}};
    CV                      cv;

    std::thread worker([&cv,&m]() {
        Lock<Pair<int,bool>> lock {m};
        auto& pair = *lock;

        while(pair.second != true) {
            lock.wait(cv);
        }
    });

    std::thread main([&cv,&m]() {
    {
        Lock<Pair<int,bool>> lock {m};
        auto& pair = *lock;
        pair.second = true;
    }
        cv.notify_all();
    });

    main.join();
    worker.join();
}

TEST(WMutex, MutexWaitingWithLambda) {
    WMutex<Pair<int,bool>>  m {{0, 0}};
    CV                      cv;

    std::thread worker([&cv,&m]() {
        Lock<Pair<int,bool>> lock {m};
        wait(cv, lock, [&lock]() { return (*lock).second; } );
    });

    std::thread main([&cv,&m]() {
        Lock<Pair<int,bool>> lock {m};
        auto& pair = *lock;

        pair.second = true;
        cv.notify_all();
    });

    main.join();
    worker.join();
}
