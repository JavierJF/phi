#include "Phi/Functional/Applicative.h"
#include "Phi/Functional/Function.h"
#include "Phi/Functional/Maybe.h"

#include <gtest/gtest.h>

using namespace Phi;
using std::vector;

using String = std::string;

//////////////////////////////// BEGIN TESTS ///////////////////////////////////

TEST( Maybe, checkType) {
    Maybe<int> number ( 9 );

    ASSERT_EQ(isNothing(number), false);
    ASSERT_EQ(isJust(number), true);
}

TEST( Maybe, copyConstructor ) {
    Maybe<int> number = 9;

    Maybe<int> otherNumber = number;
}

#if defined(__GNUG__) && !defined(__clang__)
template <template <class, class...> class M, class A, class... XS>
	requires Monad<M,A,A,XS...>
#else
template <template <class, class...> typename M, class A, class... XS>
#endif
auto checkIfJust( M<A,XS...> maybe ) {
    if( !isNothing( maybe ) ) { return 1; }
    else { return 0; }
}

TEST( Maybe, maybeShouldBeAMonad ) {
    ASSERT_EQ( 1, checkIfJust( M<Maybe, int>::ret( 5 ) ) );
}

#if defined(__GNUG__) && !defined(__clang__)
template <template <class, class...> class Ap, class A, class... XS>
    requires Applicative<Ap,A,A,XS...>
#else
template <template <class, class...> typename Ap, class A, class... XS>
#endif
auto applySequencial( Maybe<Fn<A(A)>> f, Ap<A,XS...> m ) {
    return seqApp( f, m );
}

TEST( Maybe, seqApplicativeApplication ) {
    auto action = Maybe<Fn<int(int)>>( Fn_( []( int a ) { return a++; } ) );
    auto result = applySequencial( action, Maybe<int>( 6 ) );

    ASSERT_TRUE( result == Maybe<int>( 6 ) );
}

TEST( Maybe, maybeFunctor ) {
    Maybe<int> m { 5 };

    auto res = fmap(Fn_( [](int n) { return n + 1; } ), m);

    ASSERT_EQ( fromJust( res ), 6 );
}

#if defined(__GNUG__) && !defined(__clang__)
template <template <class, class...> class F, class A, class... XS>
    requires EFunctor<F, A, A, XS...>
#else
template <class A, template <class, class...> typename F, class... XS>
#endif
auto applyFunctor( Fn<A(A)> f, F<A, XS...> m ) {
    return fmap( f, m );
}

TEST( Maybe, shouldBeFunctor ) {
    Maybe<int> m { 5 };
    Fn<int(int)> f = Fn_( [](int n) { return n + 1; } );

    auto res = applyFunctor( f, m );

    ASSERT_EQ( fromJust( res ), 6 );
}

TEST( Maybe, monadComposition ) {
    Maybe<int> m {0};

    Fn<Maybe<bool>(int)> fBool = [](int n) -> Maybe<bool> {
        if (n > 10) {
            return true;
        } else {
            return false;
        }
    };

    Fn<Maybe<String>(bool)> fString = [](bool b) -> Maybe<String> {
        if (b == true) {
            return String("true");
        } else {
            return String("false");
        }
    };

    auto res = (m >>= fBool) >>= fString;

    ASSERT_EQ( fromJust( res ), "false" );
}

TEST( Maybe, pMatchInt ) {
    Maybe<int> number { 9 };

    auto res = pMatch(
        number,
        [](int val) {
            return val + 10;
        },
        [](PNothing) {
            return 0;
        }
    );

    ASSERT_EQ(res, 19);
}

///////////////////////////////// END TESTS ////////////////////////////////////
