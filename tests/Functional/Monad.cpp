#include <Phi/Functional/Monad.h>
#include <Phi/Functional/Function.h>

#include <bits/stdint-uintn.h>
#include <gtest/gtest.h>

using namespace Phi;

TEST(doSyntaxForMonad, DoSyntax) {
    auto doRes =
        Do_(5
           // Check for ending composition
           , [](auto n) { if(n == 0) { return true; } else { return false; }}
           // Monadic operations
           , [](auto n) { return --n; }
           , [](auto n) { return --n; }
           , [](auto) { return 0; }
           , [](auto) { return +10; }
        );

    ASSERT_EQ(doRes, 0);
}

// struct cond_destructed {
//     void* vector { nullptr };
//     void(*destructor)(void*) { [] (void* vector) -> void {
//         delete static_cast<std::vector<uint64_t>*>(vector);
//     }};
//
//     cond_destructed() {
//         this->vector = new std::vector<uint64_t> {100, 10};
//     }
//
//     ~cond_destructed() {
//         if (destructor != nullptr) {
//             destructor(vector);
//         }
//     }
// };
//
// TEST(NewDeleteLeaks, ClangBug) {
//     cond_destructed foo {};
// }

// This isn't valid because you need to have an uniform return type in order
// of being able to return at any time.
// TEST(doSyntaxVariantType, DoSyntax) {
//     auto doRes =
//         Do_(5
//            // Check for ending composition <-> Can't check different types each iteration
//            , [](auto n) { if(n == 0) { return true; } else { return false; }}
//            // Monadic operations
//            , [](auto n) { if(n == 5) { return std::string("5"); }  }
//            , [](auto s) { if(s == "5") { return 10; } }
//            , [](auto n) { if(n == 10 ) { return 0; } }
//            , [](auto) { return +10; }
//         );
//
//     ASSERT_EQ(doRes, 0);
// }
