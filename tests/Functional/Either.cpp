#include <Phi/Functional/Function.h>
#include <Phi/Functional/Either.h>
#include <Phi/Basic/Bool.h>

#include <gtest/gtest.h>
#include <utility>
#include <vector>
#include <iostream>

using namespace Phi;

TEST( Either, helperMatchingFn ) {
    Either<int, std::string> hello { std::string("hello") };

    auto fInt = [](int val) -> std::string {
        return std::string("IntVal: " + std::to_string(val));
    };

    auto fString = [](std::string s) -> std::string {
        return s + " world!";
    };

    auto res = pMatch(hello, fInt, fString);

    ASSERT_EQ(res, std::string("hello world!"));
}

TEST( Either, constHelperMatchingFn ) {
    const Either<int, std::string> hello { std::string("hello") };

    auto fInt = [](int val) -> std::string {
        return std::string("IntVal: " + std::to_string(val));
    };

    auto fString = [](std::string s) -> std::string {
        return s + " world!";
    };

    auto res = pMatch(hello, fInt, fString);

    ASSERT_EQ(res, std::string("hello world!"));
}

struct APIError {
    std::string message;
    int64_t code;
};

TEST( Either, errorHandling ) {
    Either<double, APIError> res = 5.0;

    auto matchRes = [] (auto&& val) -> bool {
        using T = std::decay_t<decltype(val)>;
        if constexpr (std::is_same_v<T, double>) {
            return val > 100;
        } else if constexpr (std::is_same_v<T, APIError>) {
            return false;
        } else {
            static_assert(always_false<T>::value, "Payload: non-exhaustive visitor!");
        }
    };

    auto validPayload = pMatch(res, matchRes);

    if (!validPayload) {
        res = APIError { "Error: number too low" , -10 };
    } else {
        res = 100.0;
    }

    auto finalRes = get<APIError>( res );

    ASSERT_EQ( finalRes.code, -10 );
}

TEST( Either, functorTest ) {
    auto e = Left<Fn<int(int)>,int>( Fn_( []( int ){ return 5; } ) );

    ASSERT_EQ( fromLeft(e)(5), 5 );
}

TEST( Either, checkForLeak ) {
    auto e = Left<std::vector<int>,int>( { 1,2,3,4,5 } );

    ASSERT_EQ( fromLeft( e ), std::vector<int>( { 1,2,3,4,5 } ) );
}

TEST( Either, bindOperator ) {
    using Err = int16_t;

    auto fn = Fn_( [](const double& val) -> Either<Err, bool> {
        if (val < 50) {
            return false;
        } else {
            return true;
        }
    });

    Either<Err, double> either { 60.0 };

    auto res = either >>= fn;

    pMatch(res,
        [](Err&) {
            FAIL() << "Result shouldn't be an error.";
        },
        [](bool& val) {
            ASSERT_EQ(val, true);
        }
    );
}
