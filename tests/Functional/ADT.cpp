#include "Phi/Functional/Function.h"
#include "Phi/Functional/ADT.h"

#include <gtest/gtest.h>
#include <type_traits>
#include <cstdlib>
#include <string>
#include <typeinfo>
#include <tuple>
#include <vector>

#include <stdexcept>

#include <chrono>
#include <random>

using namespace Phi;

TEST( ADT, errorWhenRepeatingTypes ) {
    // ADT<double, double> adt { 4.0 };
}

TEST( ADT, inPlaceCtorSingleArgument ) {
    ADT<bool, double> adt { 4.0 };

    ASSERT_EQ( isType<double>(adt), true );
}

TEST( ADT, inPlaceCtorSeveralArguments ) {
    ADT<bool, double> adt { InPlaceType<double>{}, 5.0 };

    ASSERT_EQ( isType<double>(adt), true );
}

TEST( ADT, inPlaceCtorSeveralArgumentsWithInitList ) {
    ADT<bool, double, std::vector<int>> adt {
        InPlaceType<std::vector<int>>{},
        InitList<int> { 1, 2 , 3 }
    };

    ASSERT_EQ( isType<std::vector<int>>( adt ), true );
}

TEST( ADT, copyConstructorTrivialDestructor ) {
    ADT<bool, double> adt { 4.0 };

    ADT<bool, double> adtCopy( adt );

    auto matcher = [](auto&& arg)  -> bool {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, bool>) {
            return true;
        } else if constexpr (std::is_same_v<T, double>) {
            return false;
        } else {
            static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
    };

    auto res = pMatch(adtCopy, matcher);

    ASSERT_EQ( res, false );
}

TEST( ADT, copyConstructorNonTrivialDestructor ) {
    ADT<bool, double, std::string> adt { std::string("Text") };

    ADT<bool, double, std::string> adtCopy( adt );

    auto matcher = [](auto&& arg)  -> bool {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, bool>) {
            return true;
        } else if constexpr (std::is_same_v<T, double>) {
            return false;
        } else if constexpr (std::is_same_v<T, std::string>) {
            return false;
        } else {
            static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
    };

    auto res = pMatch(adtCopy, matcher);

    ASSERT_EQ( res, false );
}

TEST( ADT, copyConstructorOfBadState ) {
    ADT<bool, double, std::string> adt { std::string("Test") };

    adt = 5.0;

    auto matcher = [](auto&& arg)  -> bool {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, bool>) {
            return true;
        } else if constexpr (std::is_same_v<T, double>) {
            return false;
        } else if constexpr (std::is_same_v<T, std::string>) {
            return false;
        } else {
            static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
    };

    auto res = pMatch(adt, matcher);

    adt = std::string("Test");

    res = pMatch(adt, matcher);

    ASSERT_EQ( res, false );
}

struct MoveStruct {
    bool moved = false;

    MoveStruct() = default;
    MoveStruct(const MoveStruct& other) {
        this->moved = other.moved;
    }
    MoveStruct(MoveStruct&&) { this->moved = true; }
    MoveStruct& operator=(MoveStruct&&) { this->moved = true; return *this; }
    MoveStruct& operator=(const MoveStruct& other) { this->moved = other.moved; return *this; }
};

TEST( ADT, moveCtor ) {
    ADT<bool, double, MoveStruct> adt { MoveStruct{} };

    auto matcher = [](auto&& arg) {
        using T = std::decay_t<decltype(arg)>;
        if constexpr ( IsSameV<T, bool> ) {
            FAIL() << "Incorrect option";
        } else if constexpr ( IsSameV<T, double> ) {
            FAIL() << "Incorrect option";
        } else if constexpr ( IsSameV<T, MoveStruct> ) {
            const bool& val = arg.moved;
            ASSERT_EQ( val, true );
        } else {
            static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
    };

    pMatch( static_cast<ADT<bool, double, MoveStruct>&>( adt ), matcher );

    MoveStruct& st = const_cast<MoveStruct&>( get<MoveStruct>( adt ) );
    st.moved = false;

    ADT<bool, double, MoveStruct> adtMoved( std::move( adt ) );

    pMatch( adtMoved, matcher );
}

TEST( ADT, assigmentOfTypeWithTrivialDestructor ) {
    ADT<bool, double, std::string> adt { true };

    adt = 5.0;

    auto matcher = [](auto&& arg)  -> bool {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, bool>) {
            return true;
        } else if constexpr (std::is_same_v<T, double>) {
            return false;
        } else if constexpr (std::is_same_v<T, std::string>) {
            return false;
        } else {
            static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
    };

    auto res = pMatch(adt, matcher);

    ASSERT_EQ( res, false );
}

TEST( ADT, assigmentOfTypeWithNonTrivialDestructor ) {
    ADT<bool, double, std::string> adt { std::string("Test") };

    adt = 5.0;

    auto matcher = [](auto&& arg)  -> bool {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, bool>) {
            return true;
        } else if constexpr (std::is_same_v<T, double>) {
            return false;
        } else if constexpr (std::is_same_v<T, std::string>) {
            return false;
        } else {
            static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
    };

    auto res = pMatch(adt, matcher);

    adt = std::string("Test");

    res = pMatch(adt, matcher);

    ASSERT_EQ( res, false );
}

struct SimpleStruct {
    SimpleStruct(double) noexcept(false) {}
    SimpleStruct(const SimpleStruct&) noexcept(false) {
        throw std::runtime_error("ooooh");
    }
    SimpleStruct(SimpleStruct&&) noexcept(false) {}
    SimpleStruct& operator=(const SimpleStruct&) noexcept(false) {
        throw std::runtime_error("ooooh");
    }
};

TEST( ADT, assigmentOfTypeWithNonTrivialDestructorToBadState ) {
    ADT<bool, SimpleStruct, std::string> adt { true };

    adt = std::move( SimpleStruct { 5.0 } );

    bool badState = false;
    try {
        ADT<bool, SimpleStruct, std::string> adtCopy( adt );
    } catch(...) {
        badState = true;
    }


    ASSERT_EQ( badState, true );
}

// struct NoCopyAssigment {
//     NoCopyAssigment(double) noexcept(false) {}
//     NoCopyAssigment(const SimpleStruct&) = delete;
//     NoCopyAssigment(NoCopyAssigment&&) noexcept(false) {}
//     NoCopyAssigment& operator=(const NoCopyAssigment&) = delete;
// };
//
// TEST( ADT, errorWithNoCopyAssigment) {
//     ADT<NoCopyAssigment, double> nocopy { InPlaceType<double>{}, 4.0 };
//     // std::variant<NoCopyAssigment, double> temp = nocopy;
// }

TEST( ADT, TypeMatcher ) {
    ADT<bool,std::string,double> adt =
        ADT<bool,std::string,double>(3.0);
    auto efm =
        [] (auto&& arg) -> int {
            using T = std::decay_t<decltype(arg)>;
            if constexpr (std::is_same_v<T, bool>)
                return int(!arg);
            else if constexpr (std::is_same_v<T, std::string>)
                return int(arg.length());
            else if constexpr (std::is_same_v<T, double>)
                return int(arg + 1);
            else
                static_assert(always_false<T>::value, "non-exhaustive visitor!");
        };

     auto mres = pMatch(adt, efm);
     ASSERT_EQ( mres, 4);
}

// TODO: FIX THIS
// =============================
TEST( ADTUniqueTypes, nonRepeatedTypes ) {
    bool value = UniqueTypes<int,bool,std::string>::value;
    ASSERT_FALSE(value);
}

TEST( ADTUniqueTypes, repeatedTypes ) {
    bool value1 = UniqueTypes<int,bool,int>::value;
    ASSERT_TRUE(value1);

    bool value2 = UniqueTypes<int,int,bool>::value;
    ASSERT_TRUE(value2);

    bool value3 = UniqueTypes<int,bool,bool,int>::value;
    ASSERT_TRUE(value3);
}

TEST( ADT, TypeMatchBool ) {
    ADT<std::vector<int>, int, bool> adtBool (
        InPlaceType<std::vector<int>> {},
        std::vector<int> {0},
        std::allocator<int> {}
    );
    auto res = pMatch(adtBool, [](auto&& arg) -> int {
        using T = std::decay_t<decltype(arg)>;

        if constexpr (std::is_same_v<T, std::vector<int>>) {
            return arg[0];
        } else if constexpr (std::is_same_v<T, bool>) {
            return true;
        } else if constexpr (std::is_same_v<T, int>) {
            return 1;
        } else {
            static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
    });

    ASSERT_EQ(res, 0);
}

TEST( ADT, MatchAfterReassign ) {
    ADT<std::vector<int>, int, bool> adtVector (
        InPlaceType<std::vector<int>> {},
        std::vector<int> {0},
        std::allocator<int> {}
    );

    adtVector = true;

    auto res = pMatch(adtVector, [](auto arg) -> int {
        using t = std::decay_t<decltype(arg)>;

        if constexpr (std::is_same_v<t, std::vector<int>>) {
            return arg[0];
        } else if constexpr (std::is_same_v<t, bool>) {
            return 1;
        } else if constexpr (std::is_same_v<t, int>) {
            return 1;
        } else {
            static_assert(always_false<t>::value, "non-exhaustive visitor!");
        }
    });

    ASSERT_EQ(res, 1);
}

 TEST( ADT, unsafeGetAccess ) {
     ADT<std::vector<int>, int, bool> adtVector (
         InPlaceType<std::vector<int>> {},
         std::vector<int> {0},
         std::allocator<int> {}
     );

     auto vector = get<std::vector<int>>(adtVector);

     ASSERT_EQ(vector, std::vector<int> {0});

     bool boolVal = false;
     try {
         boolVal = get<bool>(adtVector);
     } catch (BadADTAccess&) {}

     ASSERT_EQ(boolVal, false);
 }

TEST( ADT, isTypeCheck ) {
    ADT<std::vector<int>, int, bool> adtVector (
        InPlaceType<std::vector<int>> {},
        std::vector<int> {0},
        std::allocator<int> {}
    );

    auto isVector = isType<std::vector<int>>(adtVector);

    ASSERT_EQ(isVector, true);

    adtVector = true;

    auto isBool = isType<bool>(adtVector);

    ASSERT_EQ(isBool, true);

    auto isInt = isType<int>(adtVector);

    ASSERT_EQ(isInt, false);
}

TEST( ADT, copyType ) {
    ADT<std::vector<int>, int, bool> adtVector (
        InPlaceType<std::vector<int>> {},
        std::vector<int> {0},
        std::allocator<int> {}
    );

    ADT<std::vector<int>, int, bool> vectorCopy = adtVector;

    auto isVector = isType<std::vector<int>>(vectorCopy);

    ASSERT_EQ(isVector, true);

    auto res = pMatch(vectorCopy, [](auto arg) -> int {
        using t = std::decay_t<decltype(arg)>;

        if constexpr (std::is_same_v<t, std::vector<int>>) {
            return arg[0];
        } else if constexpr (std::is_same_v<t, bool>) {
            return 1;
        } else if constexpr (std::is_same_v<t, int>) {
            return 1;
        } else {
            static_assert(always_false<t>::value, "non-exhaustive visitor!");
        }
    });

    ASSERT_EQ(res, 0);
}

TEST( ADT, withFn) {
    ADT<Fn<int(int)>, int, bool> adtVector (
        Fn_( [] (int val) -> int {
            return val + 1;
        } )
    );

    auto isFn = isType<Fn<int(int)>>(adtVector);

    ASSERT_EQ(isFn, true);
}

 TEST( ADT, tryToCopyString ) {
     ADT<std::string, bool> adtString( std::string("String") );

     ADT<std::string, bool> adtStringCopy( adtString );

     ASSERT_EQ(get<std::string>(adtStringCopy), std::string("String"));

     bool exceptionTriggered = false;
     ADT<SimpleStruct, bool> simpleStruct( SimpleStruct(5.0 ) );
     ADT<SimpleStruct, bool> simpleBool( true );
     try {
         simpleBool = simpleStruct;
     } catch (...) {
         exceptionTriggered = true;
     }

     ASSERT_EQ(exceptionTriggered, true);

     exceptionTriggered = false;

     try {
         auto sameContainedVal = pMatch(simpleBool, [](auto&& arg) -> bool {
             using t = std::decay_t<decltype(arg)>;

             if constexpr (std::is_same_v<t, SimpleStruct>) {
                 return 0;
             } else if constexpr (std::is_same_v<t, bool>) {
                 return 1;
             } else {
                 static_assert(always_false<t>::value, "non-exhaustive visitor!");
             }
         });
        ASSERT_EQ(sameContainedVal, true);
     } catch(...) {
         exceptionTriggered = true;
     }

     ASSERT_EQ(exceptionTriggered, false);

     simpleBool = true;


     auto res = get<bool>( simpleBool );
     ASSERT_EQ(res, true);
 }

struct Ss {
    Ss(const Ss&) {};
    template <typename T>
    Ss& operator=(T&&) noexcept { return *this; }
    template <typename T>
    Ss& operator=(const T&) noexcept { return *this; }
};

TEST( ADT, msvcBugTest ) {
    auto val = std::is_nothrow_assignable_v<Ss&,Ss>;
    ASSERT_EQ( val, true);
}

struct PayloadError {};

bool checkPayload(ADT<std::vector<int>,PayloadError> payload) {
    if (isType<PayloadError>(payload)) {
        return false;
    }

    auto vector = get<std::vector<int>>(payload);

    if (vector.empty() == false && vector[0] == 10) {
        return true;
    } else {
        return false;
    }
}

TEST( ADT, simplePayloadFunction ) {
    ADT<std::vector<int>, PayloadError> payload ( std::vector<int> {10} );

    auto res = checkPayload(payload);

    ASSERT_EQ(res, true);
}

TEST( ADT, pMathEFn ) {
    using std::vector;
    using std::string;

    ADT<std::vector<int>, int, bool> adtVector (
        InPlaceType<std::vector<int>> {},
        std::vector<int> {0},
        std::allocator<int> {}
    );

    auto efv =
         EFn_<int>(
             Fn<int(const vector<int>&)>([](const vector<int>& v) { return int(v.size()); }),
             Fn<int(const int&)>([](const int& l) { return l; }),
             Fn<int(const bool&)>([](const bool& i) { return int(i + 1); } )
         );

    auto res = pMatch(adtVector, efv);

    ASSERT_EQ(res, 1);

    adtVector = true;

    res = pMatch(adtVector, efv);

    ASSERT_EQ(res, 2);
}

union Thing {
    std::string str;
    std::vector<int> vector;

    Thing() {}
    ~Thing() {}
};

// Omitted parameter
TEST( ADT, fooCppTest ) {
    Thing thing;

    void* step = static_cast<void*>(&thing);
    std::vector<int>* v = static_cast<std::vector<int>*>( step );
    ::new (v) std::vector<int>();
    v->push_back(5);

    ASSERT_EQ(thing.vector.back(), 5);
}

// Omitted parameter
TEST( ADT, LinearCheckType ) {
    auto val = LinearTypeCheck<false, int, int, bool, char, std::string>::value;
    ASSERT_EQ(true, val);
}

TEST( ADT, checkCpyCtor ) {
   ADT<std::unique_ptr<bool>, int> v { std::unique_ptr<bool> { nullptr } };
}

using std::string;
using std::vector;

struct URResult {
    union {
        vector<int> result;
        string error_msg;
        double code;
    };

    int32_t index = -1;

    ~URResult() {
        if (index == 0) {
            this->result.~vector();
        } else if (index == 1) {
            this->error_msg.~basic_string();
        }
    }

    URResult(vector<int32_t> val) : result(val), index(0) {}
    URResult(string val) : error_msg(val), index(1) {}
    URResult(double val) : code(val), index(2) {}

    URResult(const URResult& other) {
        cleanContent(other.index);

        if (index == -1) {
            if (other.index == 0) {
                new (&result) vector(other.result);
            } else if (other.index == 1) {
                new (&error_msg) string(other.error_msg);
            } else {
                this->code = other.code;
            }
        } else if (index == 0) {
            if (other.index == 0) {
                this->result = other.result;
            } else if (other.index == 1) {
                new (&error_msg) string(other.error_msg);
            } else {
                this->code = code;
            }
        } else if (index == 1) {
            if (other.index == 1) {
                this->error_msg = other.error_msg;
            } else if (other.index == 0) {
                new (&result) vector(other.result);
            } else {
                this->code = code;
            }
        } else if (index == 2) {
            if (other.index == 2) {
                this->code = code;
            } else if (other.index == 1) {
                new (&error_msg) string(other.error_msg);
            } else {
                new (&result) vector(other.result);
            }
        }

        this->index = other.index;
    }

    URResult(URResult&& other) {
        if (other.index == 0) {
            new (&result) vector(std::move(other.result));
        } else if (other.index == 1) {
            new (&error_msg) string(std::move(other.error_msg));
        } else {
            this->code = other.code;
        }
        this->index = other.index;
    }

private:
    void cleanContent(int nextIndex) {
        if (index != -1) {
            if (index == 0) {
                if (nextIndex == 1 || nextIndex == 2) {
                    result.~vector();
                }
            } else if (index == 1) {
                if (nextIndex == 0 || nextIndex == 2) {
                    error_msg.~string();
                }
            }
        }
    }
};


URResult createRandomUnionResult(std::mt19937& mt, std::uniform_int_distribution<int>& dist) {
    int32_t randNum = dist(mt);
    if (randNum >= 1000/2) {
        return URResult { std::to_string(randNum) };
    } else {
        return URResult { std::vector {randNum, randNum/2} };
    }
}

using ADTRResult = ADT<vector<int>, string, double>;

ADTRResult createRandomADTResult(std::mt19937& mt, std::uniform_int_distribution<int>& dist) {
    int32_t randNum = dist(mt);
    if (randNum >= 1000/2) {
        return std::to_string(randNum);
    } else {
        return vector<int32_t> {randNum, randNum/2};
    }
}

size_t returnUnionLenght(const URResult& ures) {
    if (ures.index == 0){
        return ures.result.size();
    } else if (ures.index == 1) {
        return ures.error_msg.size();
    } else {
        return static_cast<size_t>(ures.code);
    }
}

using std::is_same_v;

size_t returnADTLenght(const ADTRResult& ures) {
    return pMatch(ures, [](auto&& arg) -> size_t {
        using t = std::decay_t<decltype(arg)>;

        if constexpr (is_same_v<t, vector<int32_t>>) {
            return arg.size();
        } else if constexpr (is_same_v<t, string>) {
            return arg.size();
        } else if constexpr (is_same_v<t, double>) {
            return static_cast<size_t>(arg);
        } else {
            static_assert(always_false<t>::value, "non-exhaustive visitor!");
        }
    });
}

void showADTResult(const ADTRResult& adtRes) {
    pMatch(adtRes, [](auto&& arg) -> void {
        using t = std::decay_t<decltype(arg)>;

        if constexpr (is_same_v<t, vector<int32_t>>) {
            std::cout << "Inner Value: " << arg[0] << "\r\n";
        } else if constexpr (is_same_v<t, string>) {
            std::cout << "Inner Value: " << arg << "\r\n";
        } else if constexpr (is_same_v<t, double>) {
            std::cout << "Inner Value: " << std::to_string(arg) << "\r\n";
        } else {
            static_assert(always_false<t>::value, "non-exhaustive visitor!");
        }
    });
}

#define RESET       "\033[0m"
#define GREEN       "\033[32m"          /* Green */

typedef std::chrono::high_resolution_clock hrc;

using std::cout;
using std::endl;

TEST( ADT, performanceAdtVsUnion ) {
    std::chrono::nanoseconds duration;
    hrc::time_point start;
    hrc::time_point end;

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist (0,1000);

    vector<URResult> unionResults {};
    uint32_t iterations = 100; // 10000000
    for (uint32_t i = 0; i < iterations; i++) {
        unionResults.push_back(createRandomUnionResult(mt, dist));
    }

    vector<ADTRResult> adtResults {};
    for (uint32_t i = 0; i < iterations; i++) {
        adtResults.push_back(createRandomADTResult(mt, dist));
    }

    start = hrc::now();

    long long uValue = 0;
    for (const auto& val : unionResults) {
        uValue += returnUnionLenght(val);
    }

    end = hrc::now();
    duration = end - start;

    cout << "[----------] " << GREEN
         << "Simple-Union Value: " << "[" << uValue << "]"
         << endl << RESET;

    cout << "[----------] " << GREEN
         << "Simple-Union " << "[" << std::to_string(duration.count()) << "]"
         << endl << RESET;

    start = hrc::now();

    long long aValue = 0;
    for (const auto& val : adtResults) {
        aValue += returnADTLenght(val);
    }

    end = hrc::now();
    duration = end - start;

    cout << "[----------] " << GREEN
         << "ADT-Union Value: " << "[" << aValue << "]"
         << endl << RESET;

    cout << "[----------] " << GREEN
         << "ADT-Union " << "[" << std::to_string(duration.count()) << "]"
         << endl << RESET;

}

TEST( ADT, performanceAdtVsUnionReverse ) {
    std::chrono::nanoseconds duration;
    hrc::time_point start;
    hrc::time_point end;

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist (0,1000);

    vector<URResult> unionResults {};
    uint32_t iterations = 100; // 10000000

    for (uint32_t i = 0; i < iterations; i++) {
        unionResults.push_back(createRandomUnionResult(mt, dist));
    }

    vector<ADTRResult> adtResults {};
    for (uint32_t i = 0; i < iterations; i++) {
        adtResults.push_back(createRandomADTResult(mt, dist));
    }

    start = hrc::now();

    long long aValue = 0;
    for (const auto& val : adtResults) {
        aValue += returnADTLenght(val);
    }

    end = hrc::now();
    duration = end - start;

    cout << "[----------] " << GREEN
         << "ADT-Union Value: " << "[" << aValue << "]"
         << endl << RESET;

    cout << "[----------] " << GREEN
         << "ADT-Union " << "[" << duration.count() << "]"
         << endl << RESET;

    start = hrc::now();

    long long uValue = 0;
    for (const auto& val : unionResults) {
        uValue += returnUnionLenght(val);
    }

    end = hrc::now();
    duration = end - start;

    cout << "[----------] " << GREEN
         << "Simple-Union Value: " << "[" << uValue << "]"
         << endl << RESET;

    cout << "[----------] " << GREEN
         << "Simple-Union " << "[" << duration.count() << "]"
         << endl << RESET;
}

//                      TESTS FOR THE EXPERIMENTAL SECTION

//                      TESTS FOR THE EXPERIMENTAL SECTION
//                    =======================================

// TEST( ADTMatch, BoolFns ) {
//     ADT<std::vector<int>, int, bool> adtBool ( std::vector<int> {0} );
//     _pMatch(adtBool,
//         [] (std::vector<int> arg) {
//             std::cout << "int with value " << arg[0] << '\n';
//         },
//         [] (int arg) {
//             std::cout << "bool with value " << arg << '\n';
//         },
//         [] (bool arg) {
//             std::cout << "bool with value " << arg << '\n';
//         }
//     );
// }
