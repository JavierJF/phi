#include "Phi/Functional/Function.h"
#include "Phi/Functional/Functor.h"

#include <gtest/gtest.h>
#include <vector>

using namespace Phi;

using std::vector;

////////////////////////////////////////////////////////////////////////////////
//                           FUNCTOR TESTS : VECTOR                           //
////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////   BEGIN   ////////////////////////////////////

TEST( FunctorTest, fmapShouldModifyVector ) {
    vector<int> v = vector<int>( {1,2,3,4} );
    auto res = fmap( Fn_( []( int v ) -> double { return v+1; } ), v );

    ASSERT_EQ( res, vector<double>( {2,3,4,5} ) );
}

#if defined(__GNUG__) && !defined(__clang__)

template <class T, template <class> class F> requires EFunctor<F,T,T>
auto testIncrement( F<T> f ) {
    return fmap( Fn_( []( T v ) -> T { return ++v; } ), f );
}

TEST( FunctorTest, shouldIncrementFunctor ) {
    auto res = testIncrement( vector<int>( {1,2,3,4} ) );

    ASSERT_EQ( res, vector<int>( {2,3,4,5} ) );
}

#else
#endif

bool filter( int a ) {
    if( a > 20 ) { return true; }
    else { return false; }
}

TEST( FunctorBoolTest, shouldCheckVectorInts ) {
    auto res = fmap( Fn_( filter ), vector<int>( {21,21,31,41} ) );

    ASSERT_EQ( res, vector<bool>( {true,true,true,true} ) );
}


////////////////////////////////  END  /////////////////////////////////////////
