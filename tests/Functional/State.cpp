#include <Phi/Functional/Function.h>
#include <Phi/Functional/State.h>
#include <Phi/Functional/Functor.h>

#include <gtest/gtest.h>
#include <vector>

using namespace Phi;
using std::vector;

///////////////////////////// MONAD OPERATIONS /////////////////////////////////

#if defined(__GNUG__) && !defined(__clang__)
template <template <class,class...> typename M, class A, class S>
    requires Monad<M,A,A,S>
#else
template <template <class,class...> typename M, class A, class S>
#endif
auto returnStateMonad( M<A,S> mA ) {
    return mA;
}

////////////////////////////////////////////////////////////////////////////////

//////////////////////////////// BEGIN TESTS ///////////////////////////////////

TEST( stateShouldBeAMonad, StateMonad) {
    auto fn =
        Fn_(
            []( vector<int> v ) {
                return Pair<int,vector<int>>( 0, v );
            }
         );

    auto st = State<int,vector<int>>( fn );
    auto f = returnStateMonad( st );
    ASSERT_EQ( 0, runState( f, vector<int>( {1,2,3} ) ).first );
}

///////////////////////////////// END TESTS ////////////////////////////////////

///////////////////////////// STACK OPERATIONS /////////////////////////////////

State<int,vector<int>> pop() {
    return State<int,vector<int>>(
        []( vector<int> v ) {
            int back = v.back();
            v.pop_back();
            return std::make_pair( back, v );
        }
    );
}

template <int I>
State<int,vector<int>> tPush( int ) {
    return State<int,vector<int>> (
        [=]( vector<int> v ) {
            v.push_back( I );
            return std::make_pair( int{}, v );
        }
    );
}

State<int,vector<int>> push( int a ) {
    return State<int,vector<int>>(
        [=]( vector<int> v ) {
            v.push_back( a );
            return std::make_pair( int{}, v );
        }
    );
}

auto stackManipIn() {
    return pop() >>= Fn_( [] (int n) {
      return tPush<3>(n) >>= Fn_( [] (int x) {
        return push( x ); }); });
}

auto stackManipOut() {
    return ( pop() >>= Fn_(tPush<3>) ) >>= Fn_(push);
}

////////////////////////////////////////////////////////////////////////////////

//////////////////////////////// BEGIN TESTS ///////////////////////////////////

TEST( stackManipIn, StateMonad) {
    vector<int> res = runState(stackManipIn(), vector<int>({1,2,3,4}) ).second;
    ASSERT_EQ( res, vector<int>( {1,2,3,3,0} ) );
}

TEST( stackManipOut, StateMonad) {
    vector<int> res = runState(stackManipOut(), vector<int>({1,2,3,4}) ).second;
    ASSERT_EQ( res, vector<int>( {1,2,3,3,0} ) );
}

///////////////////////////////// END TESTS ////////////////////////////////////
