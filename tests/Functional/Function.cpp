#include <Phi/Functional/Function.h>

#include <gtest/gtest.h>
#include <functional>
#include <iostream>
#include <chrono>
#include <list>

#define RESET       "\033[0m"
#define BLACK       "\033[30m"          /* Black */
#define RED         "\033[31m"          /* Red */
#define GREEN       "\033[32m"          /* Green */
#define YELLOW      "\033[33m"          /* Yellow */
#define BLUE        "\033[34m"          /* Blue */
#define MAGENTA     "\033[35m"          /* Magenta */
#define CYAN        "\033[36m"          /* Cyan */
#define WHITE       "\033[37m"          /* White */
#define BOLDBLACK   "\033[1m\033[30m"   /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"   /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"   /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"   /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"   /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"   /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"   /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"   /* Bold White */
#define CLEAR       "\033[2J"           /* Clear screen escape code */

using std::cout;
using std::endl;
using Phi::Fn;
using Phi::Fn_;

typedef std::chrono::high_resolution_clock hrc;

struct Show {
    Fn<std::string(Show)> show;

    Show( Fn<std::string(Show)> show ) : show( show ) {}
};

struct String : Show {
    std::string name;

    String(const char* name) : Show( [this]( Show ){ return this->name; } ) {
        this->name = std::string(name);
    }
};

std::string show( Show a ) {
   return a.show(a);
}

template <typename a>
struct Exist3 {
    Fn<a(Fn<a(Show)>)> fun;

    Exist3( Fn<a(Fn<a(Show)>)> fun ) : fun( fun ) {}

    inline constexpr a operator()( Fn<a(Show)> b ) const {
        return fun(b);
    }
};

auto sample =
    Exist3<std::string>
    (
        []( Fn<std::string(Show)> k )
        {
            return k( String( "sample" ) );
        }
    );

auto use =
    []( Exist3<std::string> f )
    {
        return f( []( auto x ){ return show(x); });
    };

int addOne( const int i ) {
    return i + 1;
}

double complex( const double a, const double b ) {
    return (a/2 + b/3)*8;
}

// GNU: GCC have a open bug which swaps the references in lambdas in certain conditions
#if !defined(__GNUG__)

TEST( crazystuff, crazy ) {
    ASSERT_EQ( use( sample ) , "sample" );
}

#endif

TEST( Fn, ctorFromPointer ) {
    Fn<int(int)> plusOne( addOne );
}

TEST( Fn, ctorFromLambda ) {
    Fn<int(int)> plusOne( []( int i ){ return i + 1; } );
}

TEST( Fn, ctorFromClosure ) {
    int cap = 5;
    Fn<int(int)> plusOne( [cap]( int i ){ return i + cap + 1; } );
}

struct Date {
    uint32_t day;
    uint32_t month;
    uint32_t year;

    Date(int d, int m, int y) : day(d), month(m), year(y) { };
    Date(const Date&) {};
};

TEST(FunctionConstruction, rvalueFunctionParam) {
    Fn<uint32_t(Date&&)> fnDate ([] (Date&& date) { return date.day; });

    ASSERT_EQ(fnDate(Date {1,2,3}), static_cast<uint32_t>(1));
}

uint32_t dateDay(Date&& date) {
    return date.day;
}

TEST(FunctionPConst, constModification) {
    Fn<uint32_t(Date&&)> fnDate ( dateDay );

    ASSERT_EQ(fnDate(Date {1,2,3}), static_cast<uint32_t>(1));
}

// TODO: Use as proof, remove std::move
TEST(FunctionPConst, movedParams) {
    Date date {1,2,3};
    Fn<uint32_t(Date&&)> fnDate ( dateDay );

    ASSERT_EQ(fnDate(std::move(date)), static_cast<uint32_t>(1));
}

TEST(FunctionLConst, constModification) {
    Fn<uint32_t(const Date&)> fnDate ([] (const Date& date) { return date.day; });

    ASSERT_EQ(fnDate(Date {1,2,3}), static_cast<uint32_t>(1));
}

TEST( Fn, execFromPointer ) {
    Fn<int(int)> plusOne( addOne );

    ASSERT_TRUE( plusOne( 4 ) == 5 );
}

TEST( Fn, execFromLambda ) {
    Fn<double(double,double)> lambda(
        [](double, double b) -> double { return b; } );

    ASSERT_TRUE( lambda( 3, 5 ) == 5 );
}

TEST( Fn, copyCtorFromLambda ) {
    Fn<int(int)> dflt( addOne );
    Fn<int(int)> dfltCopy( dflt );
}

TEST( Fn, copyCtorFromPointer ) {
    Fn<double(double,double)> fn( complex );
    Fn<double(double,double)> fnCopy( fn );
}

TEST( Fn, execCopyFromPointer ) {
    auto fn = []() {
        Fn<double(double,double)> fn( complex );
        Fn<double(double,double)> fnCopy( fn );
        return fnCopy;
    };

    ASSERT_TRUE( fn()( 2, 3 ) == 16 );
}

TEST( Fn, execCopyFromLambda ) {
    auto fn = []() {
        Fn<double(double,double)> fn( complex );
        Fn<double(double,double)> fnCopy( fn );
        return fn;
    };

    ASSERT_TRUE( fn()( 2, 3 ) == 16 );
}

inline long long complexNoDepCall(int iterations, long long initialVal) {
    long long counter = initialVal;

    for(int i = 0; i <= iterations; i++) {
        counter += static_cast<long long>(complex( i, i/3 ));
    }

    return counter;
}

inline long long complexFNoDepCall(int iterations, long long initialVal) {
    const Fn<double(double,double)> complexF { complex };
    long long counter = initialVal;

    for(int i = 0; i <= iterations; i++) {
        counter += static_cast<long long>(complexF( i, i/3 ));
    }

    return counter;
}

inline long long complexStdNoDepCall(int iterations, long long initialVal) {
    const std::function<double(double,double)> complexF { complex };
    long long counter = initialVal;

    for(int i = 0; i <= iterations; i++) {
        counter += static_cast<long long>(complexF( i, i/3 ));
    }

    return counter;
}

auto mediumValue(std::vector<long long> durations) {
    auto lenght = durations.size();
    long long count = 0;

    for (auto elem : durations) {
        count += elem;
    }

    return count / lenght;
}

TEST( FnMetrics, performanceMeasurementNoDataDep ) {
    std::chrono::nanoseconds duration;
    hrc::time_point start;
    hrc::time_point end;

    int repetitions = 10;
    std::vector<long long> durations {};
    long long counter = 0;
    uint32_t iterations = 100; // 10000000

    for (int i = 0; i <= repetitions; i++) {
        start = hrc::now();
        counter = complexNoDepCall(iterations, i);
        end = hrc::now();

        duration = end - start;
        durations.push_back(duration.count());
    }

    auto max_duration = *std::max_element(durations.begin(), durations.end());
    auto min_duration = *std::min_element(durations.begin(), durations.end());
    auto medium_duration = mediumValue(durations);

    cout << "[----------] " << GREEN
         << "Min complex: " << min_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Max complex: " << max_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Medium complex: " << medium_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Counter " << "[" << counter << "]"
         << endl << RESET;

    // Clear everything from previous measurement
    counter = 0;
    durations.clear();

    for (int i = 0; i <= repetitions; i++) {
        start = hrc::now();
        counter = complexFNoDepCall(iterations, i);
        end = hrc::now();

        duration = end - start;
        durations.push_back(duration.count());
    }

    max_duration = *std::max_element(durations.begin(), durations.end());
    min_duration = *std::min_element(durations.begin(), durations.end());
    medium_duration = mediumValue(durations);

    cout << "[----------] " << GREEN
         << "Min complexF: " << min_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Max complexF: " << max_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Medium complexF: " << medium_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Counter " << "[" << counter << "]"
         << endl << RESET;

    for (int i = 0; i <= repetitions; i++) {
        start = hrc::now();
        counter = complexStdNoDepCall(iterations, i);
        end = hrc::now();

        duration = end - start;
        durations.push_back(duration.count());
    }

    max_duration = *std::max_element(durations.begin(), durations.end());
    min_duration = *std::min_element(durations.begin(), durations.end());
    medium_duration = mediumValue(durations);

    cout << "[----------] " << GREEN
         << "Min complexF: " << min_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Max complexF: " << max_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Medium complexF: " << medium_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Counter " << "[" << counter << "]"
         << endl << RESET;

}

TEST( FnMetrics, performanceMeasurementNoDataDepReverse ) {
    std::chrono::nanoseconds duration;
    hrc::time_point start;
    hrc::time_point end;

    int repetitions = 10;
    std::vector<long long> durations {};
    long long counter = 0;
    uint32_t iterations = 100; // 10000000

    for (int i = 0; i <= repetitions; i++) {
        start = hrc::now();
        counter = complexFNoDepCall(iterations, i);
        end = hrc::now();

        duration = end - start;
        durations.push_back(duration.count());
    }

    auto max_duration = *std::max_element(durations.begin(), durations.end());
    auto min_duration = *std::min_element(durations.begin(), durations.end());
    auto medium_duration = mediumValue(durations);

    cout << "[----------] " << GREEN
         << "Min complexF: " << min_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Max complexF: " << max_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Medium complexF: " << medium_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Counter " << "[" << counter << "]"
         << endl << RESET;

    // Clear everything from previous measurement
    counter = 0;
    durations.clear();

    for (int i = 0; i <= repetitions; i++) {
        start = hrc::now();
        counter = complexNoDepCall(iterations, i);
        end = hrc::now();

        duration = end - start;
        durations.push_back(duration.count());
    }

    max_duration = *std::max_element(durations.begin(), durations.end());
    min_duration = *std::min_element(durations.begin(), durations.end());
    medium_duration = mediumValue(durations);

    cout << "[----------] " << GREEN
         << "Min complex: " << min_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Max complex: " << max_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Medium complex: " << medium_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Counter " << "[" << counter << "]"
         << endl << RESET;
}

inline long long complexDepCall(int iterations, long long initialVal) {
    long long counter = initialVal;


    for(int i = 0; i <= iterations; i++) {
        counter += (long long)complex( static_cast<double>(counter/(i + 1) + i), static_cast<double>(i/3) );
    }

    return counter;
}

inline long long complexFDepCall(int iterations, long long initialVal) {
    Fn<double(double,double)> complexF { complex };
    long long counter = initialVal;

    for(int i = 0; i <= iterations; i++) {
        counter += (long long)complex( static_cast<double>(counter/(i + 1) + i), static_cast<double>(i/3) );
    }

    return counter;
}

TEST( FnMetrics, performanceMeasurementDataDep ) {
    std::chrono::nanoseconds duration;
    hrc::time_point start;
    hrc::time_point end;

    int repetitions = 10;
    std::vector<long long> durations {};
    long long counter = 0;
    uint32_t iterations = 100; // 10000000

    for (int i = 0; i <= repetitions; i++) {
        start = hrc::now();
        counter = complexDepCall(iterations, i);
        end = hrc::now();

        duration = end - start;
        durations.push_back(duration.count());
    }

    auto max_duration = *std::max_element(durations.begin(), durations.end());
    auto min_duration = *std::min_element(durations.begin(), durations.end());
    auto medium_duration = mediumValue(durations);

    cout << "[----------] " << GREEN
         << "Min complex: " << min_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Max complex: " << max_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Medium complex: " << medium_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Counter " << "[" << counter << "]"
         << endl << RESET;

    // Clear everything from previous measurement
    counter = 0;
    durations.clear();

    for (int i = 0; i <= repetitions; i++) {
        start = hrc::now();
        counter = complexFDepCall(iterations, i);
        end = hrc::now();

        duration = end - start;
        durations.push_back(duration.count());
    }

    max_duration = *std::max_element(durations.begin(), durations.end());
    min_duration = *std::min_element(durations.begin(), durations.end());
    medium_duration = mediumValue(durations);

    cout << "[----------] " << GREEN
         << "Min complexF: " << min_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Max complexF: " << max_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Medium complexF: " << medium_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Counter " << "[" << counter << "]"
         << endl << RESET;
}

TEST( FnMetrics, performanceMeasurementDataDepReverse ) {
    std::chrono::nanoseconds duration;
    hrc::time_point start;
    hrc::time_point end;

    int repetitions = 10;
    std::vector<long long> durations {};
    long long counter = 0;
    uint32_t iterations = 100; // 10000000

    // Clear everything from previous measurement
    counter = 0;
    durations.clear();

    for (int i = 0; i <= repetitions; i++) {
        start = hrc::now();
        counter = complexFDepCall(iterations, i);
        end = hrc::now();

        duration = end - start;
        durations.push_back(duration.count());
    }

    auto max_duration = *std::max_element(durations.begin(), durations.end());
    auto min_duration = *std::min_element(durations.begin(), durations.end());
    auto medium_duration = mediumValue(durations);

    cout << "[----------] " << GREEN
         << "Min complexF: " << min_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Max complexF: " << max_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Medium complexF: " << medium_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Counter " << "[" << counter << "]"
         << endl << RESET;

    for (int i = 0; i <= repetitions; i++) {
        start = hrc::now();
        counter = complexDepCall(iterations, i);
        end = hrc::now();

        duration = end - start;
        durations.push_back(duration.count());
    }

    max_duration = *std::max_element(durations.begin(), durations.end());
    min_duration = *std::min_element(durations.begin(), durations.end());
    medium_duration = mediumValue(durations);

    cout << "[----------] " << GREEN
         << "Min complex: " << min_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Max complex: " << max_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Medium complex: " << medium_duration << endl << RESET;

    cout << "[----------] " << GREEN
         << "Counter " << "[" << counter << "]"
         << endl << RESET;

}

using std::string;

size_t sLength(string s) {
    return s.length();
}


TEST( FnCall, stdHidingNonReferenceFunctions ) {
    std::function<size_t(std::string&)> mString { sLength };
    std::string s { "C++" };

    auto res = mString(s);
    ASSERT_TRUE( res == 3 );
}

size_t sLength2(std::string s, double) {
    return s.length();
}

TEST( Fn, callingWithNonReferenceTypes2 ) {
    std::function<size_t(std::string&, double&)> mString ( sLength2 );
    std::string s { "C++" };

    double val = 5;
    auto res = mString(s, val);
    ASSERT_TRUE( res == 3 );
}

TEST( Fp, nonParameters ) {
    using Phi::Fp;

    Fp<void()> fp {
        [] () -> void {
            // Does nothing.
        }
    };

    fp();
}

TEST( Fp, partialCallNonReference ) {
    using Phi::Fp;

    Fp<double(std::string, double)> fp {[](std::string s, double) -> double {
        return static_cast<double>(s.length());
    }};

    auto fp2 = fp(std::string {"C++"});
    auto res = fp2(2.0);

    ASSERT_EQ( res, 3 );
}

// using std::function;
//
// TEST( Fp, complCallNonReference ) {
//     using Phi::Fp;
//
//     function<double(std::string&, double)> fp {[](std::string s, double) -> double {
//         return static_cast<double>(s.length());
//     }};
//
//     auto res = fp(std::string {"C++"}, 2.0);
//
//     ASSERT_EQ( res, 3 );
// }

TEST( Fp, completeCallRValueParam ) {
    using Phi::Fp;

    Fp<void(std::string&&)> fp {
        [] (string&& str) -> void {
            std::cout << "Param :" << str << "\r\n";
        }
    };

    fp(string {"C++"});
}

TEST( Fn, bindConstruction ) {
    auto binded = std::bind([](uint16_t a) -> int { return a; }, uint16_t {1});
    Phi::Fn<int()> bindedFn { binded };

    ASSERT_EQ(bindedFn(), 1);
}
