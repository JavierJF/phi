#include <gtest/gtest.h>
#include <type_traits>

#include <Phi/Basic/Bool.h>

using std::cout;
using std::endl;
using namespace Phi;

TEST( Bool, notDefaultConstructible ) {
    ASSERT_EQ( std::is_default_constructible<Bool>::value, false );
}

TEST( Bool, safeType ) {
    Bool a = True();

    ASSERT_TRUE( bool( a ) );
}
