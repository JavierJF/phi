#include <Phi/Basic/Bool.h>
#include <utility>

using namespace std;

namespace Phi
{

///////////////////////////// START BOOL TYPE //////////////////////////////////

Bool True() {
    Bool b { true };

    return b;
}

Bool False()
{
    Bool b { false };

    return b;
}

/////////////////////////////////   END   //////////////////////////////////////

}
