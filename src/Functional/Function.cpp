#include <Phi/Functional/Function.h>

#if !defined(_MSC_VER)

#include <cxxabi.h>

namespace Phi
{

std::string x_demangle(const char* mangled)
{
      int status;
      std::unique_ptr<char[], void (*)(void*)> result(
        abi::__cxa_demangle(mangled, 0, 0, &status), std::free);
      return result.get() ? std::string(result.get()) : "error occurred";
}

}

#endif
