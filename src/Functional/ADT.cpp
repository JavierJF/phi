#include <Phi/Functional/ADT.h>

namespace Phi {

BadADTState::BadADTState(const char* msg) : _msg(msg) {
}

const char* BadADTState::what() const noexcept {
    return _msg;
}

}
