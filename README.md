Phi
===

## Index

 1. Summary:
    * Resume of the content of the library and it's purpose.

 2. Priorities:
    * List of the priorities that will be followed during the development
       of this library. We hope this way users will have early knowledge
       of when certain features will be available.

 4. Usage guide:
    * Basic introduction on how to use the project.

 3. Acknowledgements:
    * Thanks to the community.

## Summary

This is a conceptual functional library using the post C++17 characteristics.
The goal is to offer some of the structures and functionalities that functional
languages have, and give examples on how to create similar structures with the
tools we have in C++.

## Priorities

 1. Ensure that the library covers the basic tools that we can find in
 functional languages, and that the library is extensible and modular.

 2. Get the maximum possible performance from the 'first class functions'.
 This type has a lot of responsibility in the functional paradigm, so,
 this will always be a key point for this library.

 3. Achieve an 'algebraic data type' that is easy to use and efficient.

 4. TBD.

## Usage

### Compile

To make use of the library it's required have the next utilities:

    - CMake, version >= 3
    - Compiler supporting at least C++17.
    - Conan, version >= 1.10
    - Doxygen

To consume the library in a project, it's only required to add the
Bintray conan repository to your remote list, and add the project
as a dependency:

    - conan remote add Philabs https://api.bintray.com/conan/javjarfer/Philabs

To compile the library itself it's only required to:

    - git clone -b develop https://gitlab.com/JavierJF/phi
    - cd phi
    - mkdir build
    - cd build

In case of Windows-Debug:

    - conan install .. -s build_type=Debug -s compiler="Visual Studio" -s compiler.runtime="MDd" --build=missing
    - cmake -G "Ninja" -DCMAKE_BUILD_TESTS=ON -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX:PATH=install -DCMAKE_CXX_COMPILER=cl -DCMAKE_C_COMPILER=cl ..
    - cmake --build . --config Debug

### Generate the docs:

Dependencies to construct the documentation with Sphinx + Breathe the docs:

    - Install python3
    - pip install sphinx sphinx_adc_theme sphinx_rtd_theme recommonmark breathe

Generate the docs:

    - cd build
    - cmake --build . --target doc; ..\doc\sphinx\make.bat html

### Launch the tests

After making the build, execute the binary:

    - build/bin/Tests

## Thanks

Thanks to all Open Source / Free Software communities, for his effort and constance.
Without then, it wouldn't be possible to access to the amount of information this
project requires. Also thanks to the C++ community, for his desires to share their
knowledge.
