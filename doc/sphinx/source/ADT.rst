ADT
======

.. doxygenstruct:: Phi::NADTStorage< false, XS... >
   :project: Phi
   :members:
   :protected-members:
   :private-members:

.. doxygenfunction:: Phi::inPlaceCtor

.. doxygenstruct:: Phi::ADT
   :project: Phi
   :members:
