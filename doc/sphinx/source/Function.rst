Fn
======

.. doxygenclass:: Phi::Fn< Out(In...)>
   :project: Phi
   :members:
   :protected-members:
   :private-members:
