Phi
===

## Índice

 1. Resumen:
    * Resumen del contenido de esta biblioteca y de su propósito.

 2. Prioridades:
    * Lista de prioridades que serán seguidas durante el desarrollo de esta
       biblioteca. Esperamos que esto de a los usuarios un conocimiento
       prematuro de cuando serán capaces de acceder a características
       de la misma en las cuales estén interesados.

 4. Guía de uso:
    * Guía de uso básica del proyecto.

 3. Agradecimientos:
    * Agradecimientos a varias comunidades.

## Resumen

Esta biblioteca funcional conceptual usando características post C++17.
El objetivo principal es ofrecer algunas de las estructuras y funcionalidades
que un lenguaje funcional posee, así como dar ejemplos de como crear
estructuras similiares con las herramientas de las que disponemos en C++,
en las cuales, no son trivialmente expresables.

## Priorities

 1. Asegurar que la biblioteca cubre las herramientas básicas que
podemos encontrar en lenguajes funcionales, y que la biblioteca es
extensible y modular.

 2. Conseguir el mayor rendimiento posible de las 'first class functions'.
 Este tipo posee mucha responsabilidad sobre el modelo de desarrollo
 'funcional', así que conseguir este punto es una gran prioridad.

 3. Conseguir un tipo de datos algebraico que sea fácil de usar y eficiente.

 4. TBD.

## Uso

### Instalación

Para usar la biblioteca es necesario contar con las siguientes utilidades:

    - CMake, versión >= 3
    - Compilador capaz de compilar al menos C++17.
    - Conan, versión >= 1.10
    - Doxygen

Para consumir la biblioteca en un proyecto, solo es necesario añadir el
repositorio de bintray donde se encuentre a tus remotes de conan,
y añadirlo como dependencia:

    - conan remote add Philabs https://api.bintray.com/conan/javjarfer/Philabs

Para compilar la biblioteca en sí misma, solo es necesario realizar los
siguientes pasos:

    - git clone -b develop https://gitlab.com/JavierJF/phi
    - cd phi
    - mkdir build
    - cd build

En caso de build para debug en Windows:

    - conan install .. -s build_type=Debug -s compiler="Visual Studio" -s compiler.runtime="MDd" --build=missing
    - cmake -G "Ninja" -DCMAKE_BUILD_TESTS=ON -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX:PATH=install -DCMAKE_CXX_COMPILER=cl -DCMAKE_C_COMPILER=cl ..
    - cmake --build . --config Debug

### Generar la documentación:

Dependencias para contruir la documentación con Sphinx + Breathe:

    - Instalar python3
    - pip install sphinx sphinx_adc_theme sphinx_rtd_theme recommonmark breathe

Generar la documentación:

    - cd build
    - cmake --build . --target doc; ..\doc\sphinx\make.bat html

### Lanzar los tests

Despues de realizar la construcción, ejecutar el binario:

    - build/bin/Tests

## Agradecimientos

Gracias a todas las comunidades de Open Source/Free Software, por su esfuerzo
y constancia. Sin ellos no hubiera sido posible acceder a la gran cantidad
de información que este proyecto requiere. También agradecer a la comunidad de
C++, por sus deseos de compartir sus conocimientos.
