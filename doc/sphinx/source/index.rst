Welcome to Phi Documentation!
=============================

.. toctree::
    :hidden:

.. page_index.rst

.. rubric:: Related Pages:
.. toctree::
    :maxdepth: 2

   README

.. rubric:: Reference and Index:

.. rubric:: System Overview:

This library is composed by several modules, each covering different general purpose
necessities in a functional way.

.. rubric:: DataType relationships:

TODO:

.. toctree::
    :maxdepth: 2

    Setting

.. toctree::
    :caption: Functional
    :maxdepth: 2

    ADT
    Function

.. toctree::
    :caption: Sync
    :maxdepth: 2

    SVar
    WMutex
    Lock

    :doc:`global`
    :ref:`genindex`
