/* -*- C++ -*- */

/**
 * @file    PTypes
 * @author  Javier Jaramago Fernández
 * @brief   Renaming of primitive types with the proposed naming convention for
 *          Phi.
 *
 *          This file gives a new naming for numeric types. The use of P letter
 *          stands for Primitive.
 */

#ifndef PTYPES
#define PTYPES

#include <cstdint>
#include <cmath>

namespace Phi
{

typedef std::uintptr_t      Addr;
typedef std::double_t       PDouble;
typedef std::float_t        PFloat;
typedef std::int8_t         PInt8;
typedef std::int16_t        PInt16;
typedef std::int32_t        PInt32;
typedef std::int64_t        PInt64;
typedef std::uint8_t        PUInt8;
typedef std::uint16_t       PUInt16;
typedef std::uint32_t       PUInt32;
typedef std::uint64_t       PUInt64;

}

#endif
