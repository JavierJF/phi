#ifndef MS_UTILS_H
#define MS_UTILS_H

#include <Phi/Functional/Either.h>

#include <utility>
#include <functional>
#include <mutex>
#include <condition_variable>

using std::move;
using std::forward;

namespace Phi {

template <typename S, typename E, typename... A>
Either<E,S> While(S&& ini, Fn<bool(const S&)> final, Fn<Either<E,S>(S&&, A&&...)> compute, A&&... a) {
    auto state { Either<E,S>( forward<S>(ini) ) };

    auto checkFinalState = [&final] (const Either<E,S>& curState) -> bool {
        return pMatch(curState,
            [] (auto&) {
                return true;
            },
            [&final] (const S& state) {
                return final(state);
            }
        );
    };

    while ( !checkFinalState(state) ) {
        state = compute( forward<S>( fromRight(state) ), forward<A>(a)... );
    }

    return state;
}

template <typename S, typename E, typename... A>
Either<E,S> WhileA(S&& ini, Fn<bool(const Either<E,S>&)> final, Fn<Either<E,S>(A&&...)> compute, A&&... a ) {
    auto state { Either<E,S> { std::forward<S>( ini ) } };

    while ( !final(state) ) {
        state = compute( std::forward<A>(a)... );
    }

    return state;
}


}

#endif
