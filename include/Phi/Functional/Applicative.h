/* -*- C++ -*- */

#ifndef APPLICATIVE
#define APPLICATIVE

#include <Phi/Functional/Function.h>
#include <Phi/Functional/Functor.h>

namespace Phi
{

template <template <class,class...> class Mn, class A, class... Pt>
struct Appl
{};

#if defined(__GNUG__) && !defined(__clang__)

template <template <class,class...> class F, class A, class B, class... Pt>
concept Applicative =  requires( F<A,Pt...> Fa
                                    , F<B,Pt...> Fb
                                    , F<Fn<B(A)>, Pt...> f )
{
    requires EFunctor<F,A,B,Pt...>;
    { seqApp( f, Fa ) } -> std::same_as<F<B,Pt...>>;
    { seqFAct( Fa, Fb ) } -> std::same_as<F<A,Pt...>>;
    { seqSAct( Fa, Fb ) } -> std::same_as<F<B,Pt...>>;
};

#else
#endif

}

#endif
