/* -*- C++ -*- */

#ifndef PHI_EITHER_H
#define PHI_EITHER_H

#include <Phi/Basic/Bool.h>

#include <Phi/Functional/ADT.h>

#include <new>
#include <optional>

namespace Phi
{

using std::bad_optional_access;

/////////////////////////// START TYPE DEFINITION //////////////////////////////

template <class Left, class Right>
struct Either : ADT<Left,Right> {
    using Base = ADT<Left,Right>;
    using Base::Base;
};

template <class _Left, class _Right>
Bool isLeft( const Either<_Left,_Right>& e ) {
    return Bool( isType<_Left>(e) );
}

template <class _Left, class _Right>
Bool isRight( const Either<_Left,_Right>& e ) {
    return Bool( isType<_Right>(e) );
}

template <class _Left, class _Rigth>
auto Left( _Left&& a ) {
    return Either<_Left,_Rigth>( std::forward<_Left>( a ) );
}

template <class _Left, class _Rigth>
auto Right( _Rigth&& b ) {
    return Either<_Left,_Rigth>( std::forward<_Rigth>( b ) );
}

template <class Left, class Rigth>
Rigth fromRight( const Either<Left,Rigth>& e ) {
    return get<Rigth>( e );
}

template <class Left, class Rigth>
Rigth fromRight( Either<Left,Rigth>&& e ) {
    return std::move( get<Rigth>( std::move( e ) ) );
}

template <class Left, class Rigth>
Left fromLeft( const Either<Left,Rigth>& e ) {
    return get<Left>( e );
}

template <class Left, class Rigth>
Left fromLeft( Either<Left,Rigth>&& e ) {
    return std::move( get<Left>( std::move( e ) ) );
}

//////////////////////////// CONVENIENTE FUNCTIONS /////////////////////////////

template <class A, class B, class Fv, class Fn>
decltype(auto) pMatch(const Either<A,B>& m, const Fv& fv, const Fn& fn) {
    auto matchFn = [&fv, &fn] (auto&& val) {
        using T = std::decay_t<decltype(val)>;
        if constexpr (std::is_same_v<T, A>) {
            return fv( forward<decltype(val)>( val ) );
        } else if constexpr (std::is_same_v<T, B>) {
            return fn( forward<decltype(val)>( val ) );
        } else {
            static_assert(always_false<T>::value, "Maybe: non-exhaustive visitor!");
        }
    };

    return pMatch( m, matchFn );
}

template <class A, class B, class Fv, class Fn>
decltype(auto) pMatch(Either<A,B>& m, const Fv& fv, const Fn& fn) {
    auto matchFn = [&fv, &fn] (auto&& val) {
        using T = std::decay_t<decltype(val)>;
        if constexpr (std::is_same_v<T, A>) {
            return fv( forward<decltype(val)>( val ) );
        } else if constexpr (std::is_same_v<T, B>) {
            return fn( forward<decltype(val)>( val ) );
        } else {
            static_assert(always_false<T>::value, "Maybe: non-exhaustive visitor!");
        }
    };

    return pMatch( m, matchFn );
}

template <class A, class B, class Fv, class Fn>
decltype(auto) pMatch(Either<A,B>&& m, const Fv& fv, const Fn& fn) {
    auto matchFn = [&fv, &fn] (auto&& val) {
        using T = std::decay_t<decltype(val)>;
        if constexpr (std::is_same_v<T, A>) {
            return fv( forward<decltype(val)>( val ) );
        } else if constexpr (std::is_same_v<T, B>) {
            return fn( forward<decltype(val)>( val ) );
        } else {
            static_assert(always_false<T>::value, "Maybe: non-exhaustive visitor!");
        }
    };

    return pMatch( std::move( m ), matchFn );
}

//////////////////////// END OF CONVENIENTE FUNCTIONS //////////////////////////

//////////////////////////// END TYPE DEFINITION ///////////////////////////////

//////////////////////////// START MONAD CONCEPT ///////////////////////////////

#if defined(__GNUG__) && !defined(__clang__)
template <template <class> class F, class E, class A, class B>
    requires Fun<F, A, Either<E,B>>
#else
template <template <class> class F, class E, class A, class B>
#endif
Either<E,B> operator>>=( Either<E,A> e, F<Either<E,B>(A)> k ) {
    if( isLeft( e ) ) { return fromLeft( e ); }
    else { return k( fromRight( e ) ); }
}

#if defined(__GNUG__) && !defined(__clang__)
template <template <class> class F, class E, class A, class B>
    requires Fun<F, A, Either<E,B>>
#else
template <template <class> class F, class E, class A, class B>
#endif
Either<E,B> operator>>=( const Either<E,A>& e, F<Either<E,B>(const A&)> k ) {
    if( isLeft( e ) ) { return fromLeft( e ); }
    else { return k( fromRight( e ) ); }
}

#if defined(__GNUG__) && !defined(__clang__)
template <class E, class A, class B>
#else
template <class E, class A, class B>
#endif
Either<E,B> operator>>( Either<E,A>, Either<E,B> k )
{
    return k;
}

template <class E, class A>
Either<E,A> ret( A a )
{
    return Right<E,A>( a );
}

///////////////////////////// END MONAD CONCEPT ////////////////////////////////

////////////////////////// START FUNCTOR CONCEPT ///////////////////////////////

#if defined(__GNUG__) && !defined(__clang__)
template <template <class> class F, class A, class B, class C>
    requires Fun<F, A, Either<A,C>>
#else
template <template <class> class F, class A, class B, class C>
#endif
Either<A,C> fmap( F<C(B)> f, Either<A,B> e )
{
    if( isLeft( e ) ) { return e; }
    else { return f( e ); }
}

#if defined(__GNUG__) && !defined(__clang__)
template <template <class> class F, class A, class B, class C>
    requires Fun<F, A, Either<A,C>>
#else
template <template <class> class F, class A, class B, class C>
#endif
Either<A,C> fmap( F<C(const B&)> f, Either<A,B> e )
{
    if( isLeft( e ) ) { return Either<A,C> ( fromLeft(e) ); }
    else { return f( fromRight( e ) ); }
}

/////////////////////////// END FUNCTOR CONCEPT ////////////////////////////////

}

#endif
