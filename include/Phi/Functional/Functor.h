/* -*- C++ -*- */

#ifndef PHI_FUNCTOR
#define PHI_FUNCTOR

#include <Phi/Functional/Function.h>

#include <vector>

namespace Phi
{

#if defined(__clang__)
#elif defined(__GNUG__)

template <template <class,class...> class F, class A, class B, class... C>
concept EFunctor = requires( Fn<B(A)> fa, F<A,C...> b )
{
    { fmap( fa, b ) } -> std::same_as<F<B,C...>>;
};

#else
#endif

#if defined(__GNUG__) && !defined(__clang__)
Fun{ F,U,T } std::vector<U> fmap( const F<U(T)>& fv, const std::vector<T>& v )
#else
template <template <class> class F, class U, class T>
std::vector<U> fmap( const F<U(T)>& fv, const std::vector<T>& v )
#endif
{
    std::vector<U> res {};

    for( auto e : v )
    {
        res.push_back( fv( e ) );
    }

    return res;
}

}

#endif
