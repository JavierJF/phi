/* -*- C++ -*- */

#ifndef PHI_MAYBE
#define PHI_MAYBE

#include <Phi/Functional/Function.h>
#include <Phi/Basic/Bool.h>

#include <Phi/Functional/Functor.h>
#include <Phi/Functional/Monad.h>
#include <Phi/Functional/ADT.h>

#include <optional>
#include <vector>
#include <iostream>

namespace Phi
{

using std::bad_optional_access;

/////////////////////////// START TYPE DEFINITION //////////////////////////////

struct PNothing {
    bool operator==(const PNothing&) const {
        return true;
    }
};

template <class A>
struct Maybe : ADT<A, PNothing> {
    using Base = ADT<A, PNothing>;
    using Base::Base;
};

template <class T>
bool isJust( const Maybe<T>& m ) {
    return isType<T>(m);
}

template <class T>
bool isNothing( const Maybe<T>& m ) {
    return isType<PNothing>(m);
}

template <class T>
T fromJust( const Maybe<T>& m ) {
    return get<T>(m);
}

template <class T>
auto Nothing() {
    return Maybe<T>( PNothing() );
}

template <class T>
auto Just( T&& value ) {
    return Maybe<T>( std::forward<T>(value) );
}

//////////////////////////// END TYPE DEFINITION ///////////////////////////////

//////////////////////////// START MONAD CONCEPT ///////////////////////////////

template <class A, class B>
Maybe<B> operator>>=( Maybe<A> ma, Fn<Maybe<B>(A)> f )
{
    if( isNothing( ma ) ) { return Nothing<B>(); }
    else { return f( fromJust( ma ) ); }
}

template <class A, class B>
Maybe<B> operator>>( Maybe<A>, Maybe<B> mb )
{
    return mb;
}

template <class T>
struct M<Maybe,T> {
    static Maybe<T> ret( T&& a ) {
        return Just( std::forward<T>( a ) );
    }
};

///////////////////////////// END MONAD CONCEPT ////////////////////////////////

/////////////////////////// START FUNCTOR CONCEPT //////////////////////////////

template <class A, class B>
Maybe<B> fmap( Fn<B(A)> f, Maybe<A> m )
{
    if( isNothing( m ) ) { return Nothing<B>(); }
    else { return Just( f( fromJust( m ) ) ); }
}

///////////////////////////// END FUNCTOR CONCEPT //////////////////////////////

///////////////////////// START APPLICATIVE CONCEPT ////////////////////////////

template <class A, class B>
Maybe<B> seqApp( Maybe<Fn<B(A)>> f, Maybe<A> a )
{
    if ( isNothing( f ) ) { return Nothing<B>(); }
    else { return fmap( fromJust( f ), a ); }
}

template <class A, class B>
Maybe<A> seqFAct( Maybe<A> a, Maybe<B> )
{
    if ( isNothing( a ) ) { return Nothing<A>(); }
    else { return a; }
}

template <class A, class B>
Maybe<B> seqSAct( Maybe<A>, Maybe<B> b )
{
    if ( isNothing( b ) ) { return Nothing<B>(); }
    else { return b; }
}

////////////////////////// END APPLICATIVE CONCEPT /////////////////////////////

template <class A, class Fv, class Fn>
ADTMatchRes<Fv,A> pMatch(const Maybe<A>& m, const Fv& fv, const Fn& fn) {
    auto matchFn = [&fv, &fn] (auto&& val) -> ADTMatchRes<Fv,A> {
        using T = std::decay_t<decltype(val)>;
        if constexpr (std::is_same_v<T, A>) {
            return fv(val);
        } else if constexpr (std::is_same_v<T, PNothing>) {
            return fn(val);
        } else {
            static_assert(always_false<T>::value, "Maybe: non-exhaustive visitor!");
        }
    };

    return pMatch(m, matchFn);
}

template <class A, class Fv, class Fn>
ADTMatchRes<Fv,A> pMatch(Maybe<A>&& m, const Fv& fv, const Fn& fn) {
    auto matchFn = [&fv, &fn] (auto&& val) -> ADTMatchRes<Fv,A> {
        using T = std::decay_t<decltype(val)>;
        if constexpr (std::is_same_v<T, A>) {
            return fv(std::forward<A>(val));
        } else if constexpr (std::is_same_v<T, PNothing>) {
            return fn(std::forward<PNothing>(val));
        } else {
            static_assert(always_false<T>::value, "Maybe: non-exhaustive visitor!");
        }
    };

    return pMatch(std::forward<Maybe<A>>(m), matchFn);
}

}

#endif
