/* -*- C++ -*- */

#ifndef MONAD
#define MONAD

#include <Phi/Functional/Function.h>
#include <Phi/Functional/Applicative.h>
#include <type_traits>

namespace Phi
{


template <template <class,class...> class Mn, class A, class... Pt>
struct M {};

#if defined(__GNUG__) && !defined(__clang__)

template <template <class,class...> class Mn, class A, class B, class... Pt>
concept Monad = requires( Mn<A,Pt...> Ma
                             , Mn<B,Pt...> Mb
                             , Fn<Mn<B,Pt...>(A)> fn
                             , A&& a)
{
    requires Applicative<Mn,A,B,Pt...>;
    { Ma >>= fn } -> std::same_as<Mn<B,Pt...>>;
    { Ma >> Mb } -> std::same_as<Mn<B,Pt...>>;
    { M<Mn,A,Pt...>::ret( std::forward<A>( a ) ) } -> std::same_as<Mn<A,Pt...>>;
};

#else
#endif

// template <class M, class F, class... Fns>
// struct Do {
//     static auto C(M m, F f, Fns... fns) {
//         return
//             m >>=
//               [=](auto val) {
//                   auto res = f(val);
//                   using nT = decltype(f(m));
//
//                   return
//                     // Don't have result yet, so cant expand to get it.
//                     res >>= Do<nT, Fns...>::C(??, ...)
//               };
//     }
// };

template <class M, class C, class... Fns>
struct Do {};

template <class M, class C, class F, class... Fns>
struct Do<M,C,F,Fns...> {
    static M N(M m, C c, F f, Fns... fns) {
        using nT = decltype(f(m));
        static_assert(
            std::is_same<M,nT>::value,
            "Do: 'F' function should return type 'M'"
        );

        if (c(m)) {
            return m;
        } else {
            auto next = f(m);

            return Do<nT,C,Fns...>::N(next, c, fns...);
        }
    }
};

template <class M, class C, class F>
struct Do<M,C,F> {
    static M N(M m, C c, F f) {
        if (c(m)) {
            return m;
        } else {
            return f(m);
        }
    }
};

template <class M, class C, class F, class... Fns>
auto Do_(M&& m, C&& c, F&& f, Fns&&... fns) {
    return Do<M,C,F,Fns...>::N(m, c, f, fns...);
}

template <class M, class C, class... Fns>
struct MDo {};

template <class M, class C, class F, class... Fns>
struct MDo<M,C,F,Fns...> {
    static M N(M m, C c, F f, Fns... fns) {
        if (c(m)) {
            return m;
        } else {
            using nT = decltype(f(m));
            auto next = m >>= f;

            return MDo<nT,C,Fns...>::N(next, c, fns...);
        }
    }
};

template <class M, class C, class F>
struct MDo<M,C,F> {
    static M N(M m, C c, F f) {
        if (c(m)) {
            return m;
        } else {
            return f(m);
        }
    }
};

template <class M, class C, class F, class... Fns>
auto MDo_(M&& m, C&& c, F&& f, Fns&&... fns) {
    return MDo<M,C,F,Fns...>::N(m, c, f, fns...);
}

}

#endif
