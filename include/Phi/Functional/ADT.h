#ifndef PHI_FUNCTIONAL_ADT_H
#define PHI_FUNCTIONAL_ADT_H

#include <Phi/Basic/Bool.h>
#include <Phi/Functional/Function.h>

#include <Std/InitList.h>
#include <Std/TypeTraits.h>
#include <Std/Utility.h>

#include <variant>

#include <algorithm>
#include <array>
#include <cstdlib>
#include <typeinfo>
#include <tuple>
#include <memory>
#include <limits>

namespace Phi
{

using std::allocator_arg;
using std::allocator_arg_t;
using std::enable_if;
using std::enable_if_t;
using std::forward;
using std::in_place_index;
using std::in_place_index_t;
using std::is_base_of_v;
using std::is_constructible;
using std::is_same;
using std::is_same_v;
using std::invoke_result_t;
using std::numeric_limits;
using std::remove_cv_t;
using std::remove_reference_t;
using std::uses_allocator;

/**
  * @brief A mixin helper to conditionally enable or disable the copy/move
  * special members.
  * @sa _Enable_special_members
  */
template<bool _Copy, bool _CopyAssignment,
         bool _Move, bool _MoveAssignment,
         typename _Tag = void>
  struct _Enable_copy_move { };
template<typename _Tag>

struct _Enable_copy_move<false, true, true, true, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = delete;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = default;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = default;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = default;
};

template<typename _Tag>
struct _Enable_copy_move<true, false, true, true, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = default;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = default;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = default;
};

template<typename _Tag>
struct _Enable_copy_move<false, false, true, true, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = delete;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = default;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = default;
};

template<typename _Tag>
struct _Enable_copy_move<true, true, false, true, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = default;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = default;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = default;
};

template<typename _Tag>
struct _Enable_copy_move<false, true, false, true, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = delete;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = default;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = default;
};

template<typename _Tag>
struct _Enable_copy_move<true, false, false, true, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = default;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = default;
};

template<typename _Tag>
struct _Enable_copy_move<false, false, false, true, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = delete;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = default;
};

template<typename _Tag>
struct _Enable_copy_move<true, true, true, false, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = default;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = default;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = default;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = delete;
};

template<typename _Tag>
struct _Enable_copy_move<false, true, true, false, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = delete;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = default;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = default;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = delete;
};

template<typename _Tag>
struct _Enable_copy_move<true, false, true, false, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = default;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = default;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = delete;
};

template<typename _Tag>
struct _Enable_copy_move<false, false, true, false, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = delete;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = default;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = delete;
};

template<typename _Tag>
struct _Enable_copy_move<true, true, false, false, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = default;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = default;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = delete;
};

template<typename _Tag>
struct _Enable_copy_move<false, true, false, false, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = delete;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = default;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = delete;
};

template<typename _Tag>
struct _Enable_copy_move<true, false, false, false, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = default;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = delete;
};

template<typename _Tag>
struct _Enable_copy_move<false, false, false, false, _Tag>
{
  constexpr _Enable_copy_move() noexcept                          = default;
  constexpr _Enable_copy_move(_Enable_copy_move const&) noexcept  = delete;
  constexpr _Enable_copy_move(_Enable_copy_move&&) noexcept       = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move const&) noexcept                    = delete;
  _Enable_copy_move&
  operator=(_Enable_copy_move&&) noexcept                         = delete;
};

class BadADTAccess: public std::exception {
private:
    const char* _msg;
public:
    BadADTAccess(const char* msg) : _msg(msg) {}
    const char *what() const noexcept {
        return _msg;
    }
};

template<typename... Ts>
struct TList {
    static constexpr std::size_t size{ sizeof... (Ts) };
};

template<typename T, typename... X>
struct IndexOf;

// IndexOf - Base case: Nothing found.
template <typename T, typename... Ts>
struct IndexOf<T, TList<Ts...>> {
    // static_assert (!(sizeof...(Ts) == 0), "IndexOf: Type not found in list.");
    static constexpr int32_t value = -1;
};

// IndexOf - Base case: Found correct type.
template <typename T, typename... Ts>
struct IndexOf<T, TList<T, Ts...>>
    : std::integral_constant<std::size_t, 0>
{};

// IndexOf - Recursive case.
template <typename T, typename TOther, typename... Ts>
struct IndexOf<T, TList<TOther, Ts...>>
    : std::integral_constant<std::size_t,1 + IndexOf<T, TList<Ts...>>::value>
{};

template <bool, class... T>
struct LinearTypeCheck : std::false_type {};

template <class... XS>
struct LinearTypeCheck<true, XS...> : std::true_type {};

template <class B, class C, class... XS>
struct LinearTypeCheck<false, B, C, XS...> : LinearTypeCheck<IsSame<B,C>::value, B, XS...> {};

template <bool, class... XS>
struct BinomialTypeCheck : std::false_type {};

template <class... XS>
struct BinomialTypeCheck<true, XS...> : std::true_type {};

template <class B, class C, class... XS>
struct BinomialTypeCheck<false,B,C,XS...> :
    BinomialTypeCheck<LinearTypeCheck<IsSame<B,C>::value, B, XS...>::value, C, XS...> {};

template <class... XS>
struct UniqueTypes;

template <class A, class... XS>
struct UniqueTypes<A,XS...> : std::false_type {};

template <class A, class B, class... XS>
struct UniqueTypes<A,B,XS...> : BinomialTypeCheck<false,A,B,XS...> {};

namespace _ADT {

using std::is_copy_constructible;

using std::is_copy_assignable;
using std::is_move_constructible;
using std::is_copy_assignable;
using std::is_move_assignable;

using std::is_nothrow_move_assignable;
using std::is_nothrow_move_constructible;

using std::is_trivially_destructible;
using std::is_trivially_copy_assignable;
using std::is_trivially_move_assignable;
using std::is_trivially_copy_constructible;
using std::is_trivially_move_constructible;

template<typename... Types>
struct Traits {
    static constexpr bool copyCtor =
        ConjunctionV<std::is_copy_constructible<Types>...>;
    static constexpr bool moveCtor =
        ConjunctionV<is_move_constructible<Types>...>;
    static constexpr bool copyAssign =
        copyCtor && moveCtor && ConjunctionV<is_copy_assignable<Types>...>;
    static constexpr bool moveAssign =
        moveCtor && ConjunctionV<is_move_assignable<Types>...>;

    static constexpr bool trivialDtor =
        ConjunctionV<is_trivially_destructible<Types>...>;
    static constexpr bool trivialCopyCtor =
        ConjunctionV<is_trivially_copy_constructible<Types>...>;
    static constexpr bool trivialMoveCtor =
        ConjunctionV<is_trivially_move_constructible<Types>...>;
    static constexpr bool trivialCopyAssign =
        trivialDtor && ConjunctionV<is_trivially_copy_assignable<Types>...>;
    static constexpr bool trivialMoveAssign =
        trivialDtor && ConjunctionV<is_trivially_move_assignable<Types>...>;

    static constexpr bool noThrowCopyCtor = false;
    static constexpr bool noThrowMoveCtor =
        ConjunctionV<is_nothrow_move_constructible<Types>...>;
    static constexpr bool noThrowCopyAssign = false;
    static constexpr bool noThrowMoveAssign =
        noThrowMoveCtor && ConjunctionV<is_nothrow_move_assignable<Types>...>;
};

template <int A, int B>
struct MT : std::conditional<(A > B), std::true_type, std::false_type> {};

template <int A, int B>
inline constexpr bool MTV = MT<A, B>::type::value;

}

struct Empty {};

template <class F, class... XS>
using ADTMatchRes =
    invoke_result_t<
        F&&,
        typename std::tuple_element<0, std::tuple<XS&&...>>::type
    >;

class BadADTState : public std::exception {
private:
    const char* _msg;
public:
    BadADTState(const char* msg);
    const char *what() const noexcept;
};

/**
 * \~english @brief
 *  Base class for storing the ADTUnion datatype.
 * \~spanish @brief
 *  Clase base para guardar el tipo ADTUnion.
 * \~english @details
 *  This class should hold the ADTUnion and should provide the following members:
 *      * Destructor: This destructor is the responsible of destroying the proper
 *          stored ADTUnion data member if it isn't trivially destructible.
 *      * Index: The current stored index of the union.
 * \~spanish @details
 *  Esta clase debería guardar el ADTUnion y debería proveer los siguientes miembros:
 *      * Destructor: Este destructor es el responsable de destruir el ADTUnion
 *          correcto si este no es trivialmente destructible.
 *      * Index: El índice actual de la unión.
 */
template <bool triviallyDestructible, class... XS>
struct NADTStorage;

namespace VStorage {

using std::forward;

template <class Union>
constexpr decltype(auto) get(in_place_index_t<0>, Union&& s) {
    return forward<Union>( s ).getFirst();
}

template <size_t N, class Union>
constexpr decltype(auto) get(in_place_index_t<N>, Union&& s) {
    return VStorage::get(in_place_index<N-1>, forward<Union>(s).rest);
}

template <size_t N, class Storage>
constexpr decltype(auto) get(Storage&& s) {
    return VStorage::get(in_place_index<N>, forward<Storage>(s).getStorage() );
}

}

template <class... XS>
union ADTUnion {};

template <class X, class... XS>
union ADTUnion<X, XS...> {
public:
    constexpr ADTUnion() : rest() {}

    template <class... Args>
    constexpr ADTUnion(in_place_index_t<0>, Args&&... args) :
        first( forward<Args>( args )... ) {}

    template <size_t N, class... Args>
    constexpr ADTUnion(in_place_index_t<N>, Args&&... args) :
        rest( in_place_index<N - 1>, forward<Args>( args )... ) {}

    ~ADTUnion() {}

     const X& getFirst() const & {
         return this->first;
     }

     X& getFirst() & {
         return this->first;
     }

     const X&& getFirst() const && {
         return std::move(this->first);
     }

     X&& getFirst() && {
         return std::move(this->first);
     }

    template <bool, class... PXS>
    friend struct NADTStorage;
private:
    X first;
    ADTUnion<XS...> rest;

    template <class Union>
    friend constexpr decltype(auto) VStorage::get(in_place_index_t<0>, Union&& s);

    template <size_t N, class Union>
    friend constexpr decltype(auto) VStorage::get(in_place_index_t<N>, Union&& s);

    template <size_t N, class Storage>
    friend constexpr decltype(auto) VStorage::get(Storage&& s);
};

template <size_t count>
using VariantIndex =
    ConditionalT<( count < static_cast<size_t>(numeric_limits<signed char>::max()) ),
        signed char,
        ConditionalT<( count < static_cast<size_t>(numeric_limits<short>::max()) ),
            short,
            int
        >
    >;

using std::destroy;
using std::index_sequence_for;

template <class... XS>
struct NADTStorage<true, XS...> {
    using VariantIndexT = VariantIndex<sizeof...(XS)>;

    static constexpr VariantIndexT invalidIndex = VariantIndexT( -1 );

    NADTStorage() : curIndex(invalidIndex) {}

    template <size_t N, class... Args>
    constexpr NADTStorage(in_place_index_t<N> index, Args&&... xs) :
        sUnion(index, std::forward<Args>(xs)...), curIndex(N) {}

    ~NADTStorage() {
        this->curIndex = this->invalidIndex;
    }

    constexpr bool isValid() const {
        return (this->curIndex != this->invalidIndex);
    }

    constexpr const VariantIndexT& currentIndex() const {
        return this->curIndex;
    }

    constexpr ADTUnion<XS...>& getStorage() & {
        return this->sUnion;
    }

    constexpr const ADTUnion<XS...>& getStorage() const & {
        return this->sUnion;
    }

protected:
    ADTUnion<XS...> sUnion;
    VariantIndexT curIndex = this->invalidIndex;

};

using std::index_sequence_for;

template <class Storage, size_t N>
static constexpr void nthDtor(Storage&& storage) {
    auto&& vari = VStorage::get<N>( storage );
    std::destroy_at( std::addressof( vari ) );
}

template <class... XS>
struct NADTStorage<false, XS...> {
    using VariantIndexT = VariantIndex<sizeof...(XS)>;

    static constexpr VariantIndexT invalidIndex = VariantIndexT( -1 );

    NADTStorage() : curIndex(invalidIndex) {}

    /**
     * \~spanish @brief
     *  Constructor que instancia el tipo contenido en el índice
     *  'N' a partir de los parámetros 'Args' proporcionados.
     */
    template <size_t N, class... Args>
    constexpr NADTStorage(in_place_index_t<N>, Args&&... xs) :
        sUnion(in_place_index_t<N>{}, std::forward<Args>(xs)...), curIndex(N) {}

    /**
    * \~spanish @brief
    *  Destruye el valor del tipo actual contenido, llamando
    *  al destructor del propio tipo.
    */
    ~NADTStorage() {
        if (this->curIndex != this->invalidIndex) {
            callDtor(index_sequence_for<XS...>{});
            this->curIndex = this->invalidIndex;
        }
    }

    /**
     * \~english @brief
     *  Checks if the current index is a valid one.
     * \~spanish @brief
     *  Comprueba si el índice actual es válido.
     * \~english @return
     *  True if the current index is valid, false otherwise.
     * \~spanish @return
     *  True si el índice actual es válido, falso en otro caso.
     */
    constexpr bool isValid() const {
        return (this->curIndex != this->invalidIndex);
    }

    /**
     * \~spanish @brief
     *  Devuelve el índice actual.
     * \~spanish @return
     *  El índice actual activo.
     */
    constexpr const VariantIndexT& currentIndex() const {
        return this->curIndex;
    }

    /**
     * \~spanish @brief
     *  Devuelve una referencia al miembro protegido sUnion, el cual
     *  encapsula el valor actual contenido.
     * @return
     *  Referencia al miembro 'sUnion'.
     */
    constexpr ADTUnion<XS...>& getStorage() & {
        return this->sUnion;
    }

    /**
     * \~spanish @brief
     *  Devuelve una referencia constante al miembro protegido sUnion,
     *  el cual el valor actual contenido.
     * @return
     *  Referencia constante al miembro 'sUnion'.
     */
    constexpr const ADTUnion<XS...>& getStorage() const & {
        return this->sUnion;
    }

protected:
    /**
     * \~spanish @brief
     *  Alias para el tipo de un puntero que representa a una especialización
     * de la función 'nthDtor'.
     */
    using dtorFnType = void(*)(const NADTStorage&);

    /**
     * \~spanish @brief
     *  Miembro estático que consiste en un array de punteros a cada una
     *  de las posibles especializaciones de la función
     *  'nthDtor', con un tamaño total igual al número de elementos
     *  que son contenidos en el tipo.
     */
    template <std::size_t... indices>
    static constexpr dtorFnType dtorTable[sizeof...(indices)] { &nthDtor<const NADTStorage&, indices>... };

    /**
     * \~spanish @brief
     *  Helper function miembra que sirve para poder expandir la lista
     *  de índices a través de su parámetro, para así poder especializar
     *  el array miembro 'dtorTable', y llamar al destructor correspondiente
     *  al tipo actual contenido.
     */
    template <std::size_t... indices>
    constexpr void callDtor(std::index_sequence<indices...>) {
        dtorFnType const * ptrToTable = &dtorTable<indices...>[0];
        ptrToTable[this->curIndex](*this);
    }

    /**
     * @brief Contenedor que guarda una de las posibles alternativas
     *  que el tipo puede encapsular.
     */
    ADTUnion<XS...> sUnion;
    /**
     * @brief Índice del elemento actual contenido en el tipo.
     */
    VariantIndexT curIndex = this->invalidIndex;
};

template <class... XS>
using NADTStorageT = NADTStorage<_ADT::Traits<XS...>::trivialDtor, XS...>;

/**
 * \~english @brief
 *  Function that represents the generic in place constructor.
 * \~spanish @brief
 *  Función que representa el constructor genérico 'in place'.
 * \~english @details
 *  The function takes the pointers from the source an target
 *  memory locations of the variant storage.
 *
 *  There are several key parts to explain from this function.
 *
 *   - 1. The casts:
 *      Source and target need to be casted to its underlaying types
 *      in order to call the proper copy constructor.
 *
 *      For both types, we remove references if they have. Creating
 *      the 'pSrcType' and 'pTargetType' (in which "p" stands for "plain").
 *
 *      We now cast the source pointer to the new 'pSrcType' for then
 *      casting its content to the original 'srcType' and passing it
 *      to the new 'pTargetType'.
 * \~spanish @details
 *  La función toma como parámetros punteros a las direcciones
 *  de memoria de origen y destino del contendor del variant.
 *
 *  Hay varias partes claves para explicar de esta función:
 *
 *  - 1. Los casteos:
 *      El origen y destino tienen que ser casteado a sus tipos
 *      subyacentes para poder llamar al constructor apropiado.
 *
 *      Para ambos tipos, debemos eliminar las referencias si
 *      estos las possen, creando los tipos 'pSrcType' y
 *      'pTargetType' ('p' aquí significa 'plain').
 *
 *      Después, debemos castear el tipo del puntero a el nuevo
 *      'pSrcType' para después castear de nuevo el contenido
 *      de ese puntero al tipo original 'srcType'.
 *
 *  - 2. Construcción in-place
 *      Una vez mediante los casteos, hemos conseguido tener
 *      'el tipo original' que quería ser pasado al constructor,
 *      debemos 'forwardear' su valor al constructor del tipo
 *      'pTargetType', pero usando el in-place constructor '::new".
 */
template <class srcType, class targetType>
static constexpr void inPlaceCtor(void* src, void* target) {
    using pSrcType = remove_reference_t<srcType>;
    using pTargetType = remove_reference_t<targetType>;

    ::new(target) pTargetType( forward<srcType>( static_cast<srcType>( *( static_cast<pSrcType*>(src) ) ) ) );
}

using ctorFnType = void(*)(void*, void*);

template <bool, class... XS>
struct CopyConstructorBase : NADTStorageT<XS...> {
    using Base = NADTStorageT<XS...>;
    using Base::Base;

    CopyConstructorBase(const CopyConstructorBase& other) : Base() {
         if ( other.isValid() ) {
             auto thisUnionAddress = std::addressof(this->sUnion);
             auto otherUnionAddress = std::addressof(other.sUnion);

             constexpr ctorFnType ctorTable[] = { &inPlaceCtor<const XS&, XS&>... };
             // Note: GCC traits the object as a 'const' for later
             // removing the 'constness' property using a const_cast
             // to the pointer, don't really know if this is simply
             // an abuse of the type system for being able to access
             // 'no matter what' to the property through only a
             // particular method.
             ctorTable[other.curIndex] (
                 static_cast<void*>( const_cast<ADTUnion<XS...>*>( otherUnionAddress ) ),
                 static_cast<void*>(thisUnionAddress)
             );
             this->curIndex = other.curIndex;
         }
    }

    CopyConstructorBase() = default;
    CopyConstructorBase(CopyConstructorBase&&) = default;
    CopyConstructorBase& operator=(const CopyConstructorBase&) = default;
    CopyConstructorBase& operator=(CopyConstructorBase&&) = default;

private:

    // static constexpr ctorFnType ctorTable[] = { &inPlaceCtor<const XS&, XS&>... };
};

template <class... XS>
struct CopyConstructorBase<true, XS...> : NADTStorageT<XS...> {
    using Base = NADTStorageT<XS...>;
    using Base::Base;
};

template <class... XS>
using CopyConstructorBaseT = CopyConstructorBase<_ADT::Traits<XS...>::trivialCopyCtor, XS...>;

template <bool, class... XS>
struct MoveConstructorBase : CopyConstructorBaseT<XS...> {
    using Base = CopyConstructorBaseT<XS...>;
    using Base::Base;

    MoveConstructorBase(MoveConstructorBase&& other) {
        if ( other.isValid() ) {
            auto thisUnionAddress = std::addressof(this->sUnion);
            auto otherUnionAddress = std::addressof(other.sUnion);

            ctorTable[other.curIndex] (
                static_cast<void*>(otherUnionAddress),
                static_cast<void*>(thisUnionAddress)
            );

            this->curIndex = other.curIndex;
        }
    }

    MoveConstructorBase(const MoveConstructorBase&) = default;
    MoveConstructorBase& operator=(const MoveConstructorBase&) = default;
    MoveConstructorBase& operator=(MoveConstructorBase&&) = default;

private:
    static constexpr ctorFnType ctorTable[] = { &inPlaceCtor<XS&&, XS&>... };
};

template <class... XS>
struct MoveConstructorBase<true, XS...> : CopyConstructorBaseT<XS...> {
    using Base = CopyConstructorBaseT<XS...>;
    using Base::Base;
};

template <class... XS>
using MoveConstructorBaseT = MoveConstructorBase<_ADT::Traits<XS...>::trivialMoveCtor, XS...>;

/**
 * \~english @brief
 *  Function to cast pointers to references to Types. Used to access the classes member
 *  functions of the types hidden inside opaque "void" pointers.
 * \~spanish @brief
 *  Función para castear punteros a referencias a tipos. Usada para acceder a los miembros
 *  de clase escondido a través de los punteros opacos "void".
 * \~english @details
 *  Marking the return type as a reference shouldn't be necessary but it seens like MSVC
 *  isn't able to detect that all our use cases are references and produces warnings that
 *  shouldn't be there.
 * \~spanish @details
 *  Marcar el tipo de retorno como referencia no debería ser necesario, pero parece que
 *  el MSVC no es capaz de detectgar que todos los casos de usos son referencias,
 *  y produce warnings que preferimos evitar.
 */
template <class Type>
inline constexpr Type& pointerToType(void* ptr) {
    return static_cast<Type&>( * static_cast<remove_reference_t<Type>*>(ptr)  );
}

/**
 * \~spanish @brief
 *  Función análoga a la función 'inPlaceCtor' pero para realizar una
 *  asignación en lugar de una 'construción in-place'.
 *
 * \~spanish @details
 *  Las técnicas usadas para realizar la operación son análogas a las vistas
 *  en la función 'inPlaceCtor'.
 */
template <class srcType, class targetType>
inline constexpr void inPlaceAssign(void* src, void* target) {
    pointerToType<targetType>( target ) = pointerToType<srcType>( src );
}

template <bool, class... XS>
struct CopyAssignBase : MoveConstructorBaseT<XS...> {
    using Base = MoveConstructorBaseT<XS...>;
    using Base::Base;

    CopyAssignBase& operator=(const CopyAssignBase& other) {
        if ( other.isValid() ) {
            auto thisUnionAddress = std::addressof(this->sUnion);
            auto otherUnionAddress = std::addressof(other.sUnion);
            constexpr ctorFnType assignTable[] = { &inPlaceAssign<const XS&, XS&>... };

            if (this->curIndex == other.curIndex) {
                assignTable[this->curIndex] (
                    static_cast<void*>( const_cast<ADTUnion<XS...>*>( otherUnionAddress ) ),
                    static_cast<void*>( thisUnionAddress )
                );
            } else {
                CopyAssignBase temp( std::move( other ) );
                this->~CopyAssignBase();

                // Rethrow the exception again so user can realize of the
                // error from the type that was being copy.
                try {
                    ::new (this) CopyAssignBase( temp );
                } catch(...) {
                    this->curIndex = this->invalidIndex;
                    throw;
                }
            }
        }

        return *this;
    }

    CopyAssignBase(const CopyAssignBase&) = default;
    CopyAssignBase(CopyAssignBase&&) = default;
    CopyAssignBase& operator=(CopyAssignBase&&) = default;
private:

    // static constexpr ctorFnType assignTable[] = { &inPlaceAssign<const XS&, XS&>... };
};

template <class... XS>
struct CopyAssignBase<true, XS...> : MoveConstructorBaseT<XS...> {
    using Base = MoveConstructorBaseT<XS...>;
    using Base::Base;
};

template <class... XS>
using CopyAssignBaseT = CopyAssignBase<_ADT::Traits<XS...>::trivialCopyAssign, XS...>;

template <bool, class... XS>
struct MoveAssignBase : CopyAssignBaseT<XS...> {
    using Base = CopyAssignBaseT<XS...>;
    using Base::Base;

    MoveAssignBase& operator=(MoveAssignBase&& other) {
        if ( other.isValid() ) {
            if (this->curIndex == other.curIndex) {
                auto thisUnionAddress = std::addressof(this->sUnion);
                auto otherUnionAddress = std::addressof(other.sUnion);
                constexpr ctorFnType assignTable[] = { &inPlaceAssign<const XS&, XS&>... };

                assignTable[this->curIndex](
                    static_cast<void*>( const_cast<ADTUnion<XS...>*>( otherUnionAddress ) ),
                    static_cast<void*>( thisUnionAddress )
                );
            } else {
                MoveAssignBase temp( std::move(other) );
                this->~MoveAssignBase();

                try {
                    ::new (this) MoveAssignBase( std::move( temp ) );
                } catch(...) {
                    this->curIndex = this->invalidIndex;
                    throw;
                }
            }
        }

        return *this;
    }

    MoveAssignBase(const MoveAssignBase&) = default;
    MoveAssignBase(MoveAssignBase&&) = default;
    MoveAssignBase& operator=(const MoveAssignBase&) = default;
private:

    // static constexpr ctorFnType assignTable[] = { &inPlaceAssign<XS&&, XS&>... };
};

template <class... XS>
struct MoveAssignBase<true, XS...> : CopyAssignBaseT<XS...> {
    using Base = CopyAssignBaseT<XS...>;
    using Base::Base;
};

template <class... XS>
using MoveAssignBaseT = MoveAssignBase<_ADT::Traits<XS...>::trivialMoveAssign, XS...>;

template <class... XS>
struct ADTBaseT : MoveAssignBaseT<XS...> {
    using Base = MoveAssignBaseT<XS...>;

    template <size_t N, class X, class... PXS>
    ADTBaseT(in_place_index_t<N> index, X&& x, PXS&&... xs) :
        Base(index, forward<X>( x ), forward<PXS>( xs )...) {}

    ADTBaseT(const ADTBaseT&) = default;
    ADTBaseT(ADTBaseT&&) = default;
    ADTBaseT& operator=(const ADTBaseT&) = default;
    ADTBaseT& operator=(ADTBaseT&&) = default;
};

template <class... XS>
struct ADT;

template <class X, class... XS>
constexpr decltype(auto) get(ADT<XS...>&& adt);

template <class X, class... XS>
constexpr decltype(auto) get(const ADT<XS...>& adt);

template <class... XS>
struct ADT :
    ADTBaseT<XS...>,
    private _Enable_copy_move<
      _ADT::Traits<XS...>::copyCtor,
      _ADT::Traits<XS...>::copyAssign,
      _ADT::Traits<XS...>::moveCtor,
      _ADT::Traits<XS...>::moveAssign,
      ADT<XS...>
    >
{
    static_assert(
        !UniqueTypes<XS...>::value,
        "ADT: Types should be unique."
    );
    static_assert(
        !ConjunctionV<std::is_reference<XS>...>,
        "ADT: Types must have no reference."
    );
    static_assert(
        !ConjunctionV<std::is_void<XS>...>,
        "ADT: Types must have no void."
    );
    using Base = ADTBaseT<XS...>;

    /**
     * \~spanish @brief
     *  Eliminado el default constructor del tipo. Cualquier intento
     *  de instanciar el tipo debe hacerse a través del resto de
     *  constructores disponibles.
     */
    ADT() = delete;
    /**
     * \~spanish @brief
     *  Destructor predeterminado al definido en la clase base.
     */
    ~ADT() = default;
    /**
     * \~spanish @brief
     *  Copy constructor predeterminado al definido en la clase base.
     */
    ADT(const ADT&) = default;
    /**
     * \~spanish @brief
     *  Move constructor predeterminado al definido en la clase base.
     */
    ADT(ADT&&) = default;
    /**
     * \~spanish @brief
     *  Copy asignment operator predeterminado al definido en la clase base.
     */
    ADT& operator=(const ADT&) = default;
    /**
     * \~spanish @brief
     *  Move asignment operator predeterminado al definido en la clase base.
     */
    ADT& operator=(ADT&&) = default;

    /**
     * \~spanish @brief
     *  'Constructor de conversión'. Construye un ADT conteniendo la alternativa
     *  T.
     */
    template <
        class T,
        EnableIfT<
            !IsSameV< RmCVRefT<T>, ADT >
            && !is_base_of_v< ADT, RmCVRefT<T> >
            && _ADT::MTV<sizeof...(XS), 0>
            && IsConstructibleV<T&&, T&&>,
            bool
        > = true
    >
    constexpr ADT(T&& t) noexcept(IsNoThrowConstructibleV<T&&, T&&>) :
        ADTBaseT<XS...>( InPlaceIndex< IndexOf<T, TList<XS...>>::value > {}, forward<T>( t ) ) {}

    /**
     * \~spanish @brief
     *  Construye una alternativa del ADT con los valores proporcionados en Args,
     *  para el tipo especificado X.
     *
     * \~spanish @details
     *  Este constructor nos facilita el poder construir un tipo X de las posibles
     *  alternativas cuando este necesita múltiples parámetros para ser construido.
     */
    template <
        class X,
        class... Args,
        EnableIfT<
            LinearTypeCheck<false, X, Args...>::value
            && IsConstructibleV<X, Args...>,
            bool
        > = true
    >
    constexpr explicit ADT(InPlaceType<X>, Args&&... xs) noexcept(IsNoThrowConstructibleV<X, Args...>) :
        ADTBaseT<XS...>( InPlaceIndex< IndexOf<X, TList<XS...>>::value > {}, forward<Args>( xs )... ) {}

    /**
     * \~spanish @brief
     *  Construye una alternativa del ADT con los valores proporcionados en Args, y
     *  en la lista InitList<U> para el tipo especificado X.
     *
     * \~spanish @details
     *  Este constructor nos facilita el poder construir un tipo X de las posibles
     *  alternativas cuando este necesita múltiples parámetros para ser construido,
     *  y además recibe una "initializer-list".
     */
    template <
        class X,
        class U,
        class... Args,
        EnableIfT<
            LinearTypeCheck<false, X, XS...>::value
            && IsConstructibleV<X, InitList<U>, Args...>,
            bool
        > = true
    >
    constexpr explicit ADT( InPlaceType<X>, InitList<U> il, Args&&... xs )
         noexcept(IsNoThrowConstructibleV<X&&, InitList<U>, Args&&...>) :
            ADTBaseT<XS...>(
                InPlaceIndex< IndexOf<X, TList<XS...>>::value > {},
                forward<InitList<U>>( il ),
                forward<Args>( xs )...
            ) {}

    /**
     * \~spanish @brief
     * 'Asignación de conversión'. Determina una alternativa viable que será
     *  seleccionada por sobrecarga para el tipo proporcionado T, e instancia
     *  la alternativa conveniente.
     *
     */
    template <
        class T,
        EnableIfT<
            !IsSameV<RmCVRefT<T>, ADT>
            && !is_base_of_v< ADT, RmCVRefT<T> >
            && IndexOf<RmCVRefT<T>, TList<XS...>>::value != -1
            && IsConstructibleV<T, T>
            && std::is_assignable_v<T&, T>,
            bool
        > = true
    >
    constexpr ADT& operator=(T&& arg) {
        constexpr size_t index = IndexOf<RmCVRefT<T>, TList<XS...>>::value;

        if (index == this->currentIndex()) {
            auto& storage = VStorage::get( in_place_index<index>, this->getStorage() );

            storage = forward<T>( arg );

            this->curIndex = index;
        } else {
            constexpr bool noThrowCtor = IsNoThrowConstructibleV<T, T>;
            constexpr bool noThrowMove = IsNoThrowMoveConstructible<T>::value;

            if constexpr( noThrowCtor && noThrowMove ) {
                T& storage = VStorage::get( in_place_index<index>, this->getStorage() );

                void* srcPtr = static_cast<void*>(&arg);
                void* dstPtr = static_cast<void*>(&storage);

                inPlaceCtor<T&&, T&>( srcPtr,  dstPtr );

                this->curIndex = index;
            } else {
                *this = ADT( forward<T>( arg ) );
            }
        }

        return *this;
    }

    template <class A>
    static bool equalFn(void* firstPtr, void* secondPtr) {
        const ADT& first = pointerToType<const ADT&>( firstPtr );
        const ADT& second = pointerToType<const ADT&>( secondPtr );

        auto& firstStg = get<A>( first );
        auto& secondStg = get<A>( second );

        return firstStg == secondStg;
    }

    /**
     * \~spanish @brief
     *  Operador de comparación de igualdad. Compara tipos ADT comparando
     *  primero si estos tienen la misma alternativa activa, si es así,
     *  usa el operador de comparación sobre los tipos guardados
     *  en dicha alternativa en ambos ADT.
     * \~spanish @param other
     *  El otro ADT a ser comparado.
     * \~spanish @return
     *  True en caso de que ambos tipos sean iguales, falso en otro caso.
     */
    bool operator==(const ADT& other) const {
        using equalFnType = bool(*)(void*, void*);
        constexpr equalFnType  equalFnTable[] = { &equalFn<XS>... };

        if ( this->currentIndex() == other.currentIndex() ) {
            return equalFnTable[this->currentIndex()] (
                static_cast<void*>( const_cast<ADT*>( this ) ),
                static_cast<void*>( const_cast<ADT*>( &other ) )
            );
        } else {
            return false;
        }
    }

    template <class A>
    static ADT ret(A&& a) {
        return ADT( std::forward<A>( a ) );
    }

private:
    /**
     * \~spanish @brief
     *  Helper function usada para "matchear" con el contenido del ADT
     *  a través de una función proporcionada que debe ser capaz de
     *  recibir cada uno de los distintos tipos que el ADT puede contener.
     * \~spanish @param adt
     *  Referencia a el tipo ADT contra el que se va a hacer el pattern matching.
     * \~spanish @param fn
     *  Referencia constante al tipo 'callable' que va a acceder al contenido
     *  del tipo ADT.
     * \~spanish @return
     *  El valor de retorno que la función devuelve al ejecutarse sobre
     *  el contenido del tipo ADT.
     */
    template <class F, class... PXS>
    decltype(auto) pMatch(ADT<XS...>& adt, const F& fn);

    /**
     * \~spanish @brief
     *  Igual que 'pMatch' pero el ADT se recibe a traves de una referencia
     *  constante.
     * \~spanish @param adt
     *  Referencia a el tipo ADT contra el que se va a hacer el pattern matching.
     * \~spanish @param fn
     *  Referencia constante al tipo 'callable' que va a acceder al contenido
     *  del tipo ADT.
     * \~spanish @return
     *  El valor de retorno que la función devuelve al ejecutarse sobre
     *  el contenido del tipo ADT.
     */
    template <class F, class... PXS>
    decltype(auto) pMatch(const ADT<XS...>& adt, const F& fn);

    /**
     * \~spanish @brief
     *  Igual que 'pMatch' pero el ADT se recibe a traves de una rvalue
     *  reference.
     * \~spanish @param adt
     *  Referencia a el tipo ADT contra el que se va a hacer el pattern matching.
     * \~spanish @param fn
     *  Referencia constante al tipo 'callable' que va a acceder al contenido
     *  del tipo ADT.
     * \~spanish @return
     *  El valor de retorno que la función devuelve al ejecutarse sobre
     *  el contenido del tipo ADT.
     */
    template <class F, class... PXS>
    decltype(auto) pMatch(ADT<XS...>&& adt, const F& fn);

    /**
     * \~spanish @brief
     *  Comprueba si el ADT contiene la alternativa de tipo X.
     * \~spanish @param adt
     *  El tipo ADT cuyo actual alternativa se va a comprobar.
     * \~spanish @return
     *  True si el tipo X es la alternativa actualmente contenida
     *  en el tipo X, false si no.
     */
    template <class X, class... PXS>
    friend bool isType(const ADT<PXS...>& adt);

    /**
     * \~spanish @brief
     *  Comprueba si el ADT no contiene ningún valor.
     * \~spanish @details
     *  Este estado es posible en este tipo, solamente si se
     *  ha producido alguna excepción en algún constructor,
     *  que ha provocado que no se haya inicializado correctamente.
     * \~spanish @param adt
     *  El tipo ADT a comprobar.
     * \~spanish @return
     *  True si el ADT no contiene ningún valor,
     *  false otherwise.
     */
    template <class... PXS>
    friend bool valueless(const ADT<PXS...>& adt);
};

template <class X, class... XS>
constexpr decltype(auto) get(ADT<XS...>&& adt) {
    constexpr size_t index = IndexOf<X, TList<XS...>>::value;

    if (index  == adt.currentIndex()) {
        return VStorage::get(in_place_index<index>, adt.getStorage() );
    } else {
        throw BadADTAccess("ADT: Trying to access to not currently holded type.");
    }
}

template <class X, class... XS>
constexpr decltype(auto) get(const ADT<XS...>& adt) {
    constexpr size_t index = IndexOf<X, TList<XS...>>::value;

    if (index  == adt.currentIndex()) {
        return VStorage::get(in_place_index<index>, adt.getStorage() );
    } else {
        throw BadADTAccess("ADT: Trying to access to not currently holded type.");
    }
}

template <class ResType, class F, class X, class... XS>
constexpr ResType matchADT(ADT<XS...>& adt, const F& fn) {
    constexpr size_t index = IndexOf<X, TList<XS...>>::value;

    return fn( VStorage::get<index>( adt ) );
}

template <class ResType, class F, class X, class... XS>
constexpr ResType cMatchADT(const ADT<XS...>& adt, const F& fn) {
    constexpr size_t index = IndexOf<X, TList<XS...>>::value;

    return fn( VStorage::get<index>( adt ) );
}

template <class ResType, class F, class X, class... XS>
constexpr ResType rMatchADT(ADT<XS...>&& adt, const F& fn) {
    constexpr size_t index = IndexOf<X, TList<XS...>>::value;

    return fn( std::move( VStorage::get<index>( adt ) ) );
}

template <class ResType, class F, class... XS>
struct CADTDispatchTable {
    using matchFn = ResType(*)(const ADT<XS...>&, const F&);
    static constexpr matchFn table[] = { &cMatchADT<ResType, F, XS, XS...>... };
};

template <class ResType, class F, class... XS>
struct ADTDispatchTable {
    using matchFn = ResType(*)(ADT<XS...>&, const F&);
    static constexpr matchFn table[] = { &matchADT<ResType, F, XS, XS...>... };
};

template <class ResType, class F, class... XS>
struct RADTDispatchTable {
    using matchFn = ResType(*)(ADT<XS...>&&, const F&);
    static constexpr matchFn table[] = { &rMatchADT<ResType, F, XS, XS...>... };
};

/**
 * Matching should obey several properties, including:
 *     + Always returning the same type.
 *     + Ensuring the function is callable with each possible type.
 */
template <class F, class... XS>
decltype(auto) pMatch(ADT<XS...>& adt, const F& fn) {
    using VariantIndexT = VariantIndex<sizeof...(XS)>;
    const VariantIndexT& index = adt.currentIndex();
    using ResType = decltype( fn(VStorage::get<0>( adt )) );

    if (index == adt.invalidIndex) {
        throw BadADTState("ADTBadState: ADT is in a valueless state.");
    }

    return ADTDispatchTable<ResType, F, XS...>::table[index]( adt, fn );
}

template <class F, class... XS>
decltype(auto) pMatch(const ADT<XS...>& adt, const F& fn) {
    using VariantIndexT = VariantIndex<sizeof...(XS)>;
    const VariantIndexT& index = adt.currentIndex();
    using ResType = decltype( fn(VStorage::get<0>( adt )) );

    if (index == adt.invalidIndex) {
        throw BadADTState("ADTBadState: ADT is in a valueless state.");
    }

    return CADTDispatchTable<ResType,F,XS...>::table[index](adt, fn);
}

template <class F, class... XS>
decltype(auto) pMatch(ADT<XS...>&& adt, const F& fn) {
    using VariantIndexT = VariantIndex<sizeof...(XS)>;
    const VariantIndexT& index = adt.currentIndex();
    using ResType = decltype( fn( std::move( VStorage::get<0>( adt ) ) ) );

    if (index == adt.invalidIndex) {
        throw BadADTState("ADTBadState: ADT is in a valueless state.");
    }

    return RADTDispatchTable<ResType,F,XS...>::table[index]( std::move(adt), fn );
}

template<class T> struct always_false : std::false_type {};

template <class X, class... XS>
bool isType(const ADT<XS...>& adt) {
    int32_t index = static_cast<int32_t>( IndexOf<X, TList<XS...>>::value );

    if (adt.curIndex == index) {
        return true;
    } else {
        return false;
    }
}

template <class... PXS>
bool valueless(const ADT<PXS...>& adt) {
    return (adt.curIndex == adt.invalidIndex);
}

}

#endif // VARIANT
