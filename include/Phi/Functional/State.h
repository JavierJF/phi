/* -*- C++ -*- */

#ifndef STATE_MONAD
#define STATE_MONAD

#include <Phi/Functional/Function.h>
#include <Phi/Functional/Monad.h>
#include <Phi/Functional/Maybe.h>

#include <Std/Utility.h>

#include <vector>
#include <tuple>

namespace Phi
{

using std::vector;

/////////////////////////// START TYPE DEFINITION //////////////////////////////

template <class A, class S>
struct State {
    Fn<Pair<A,S>(S)> runState;

    State( Fn<Pair<A,S>(S)> f ) : runState( f ) {}
};

template <class A, class S>
auto runState( State<A,S> st, S s ) {
    return st.runState( s );
}
//////////////////////////// END TYPE DEFINITION ///////////////////////////////

//////////////////////////// START MONAD CONCEPT ///////////////////////////////

// (>>=) :: State s a -> (a -> State s b) -> State s b
template <class S, class A, class B>
State<B, S> operator>>=( State<A, S> st, Fn<State<B, S>(A)> fn ) {
    return
        State<B,S> (
            [=]( S state ) {
                auto newStatePair = st.runState( state );
                auto newState = fn( newStatePair.first );
                return newState.runState( newStatePair.second );
            }
        );
}

template <class S, class A, class B>
State<B,S> operator>>( State<A,S>, State<B,S> stB ) {
    return stB;
}

template <class A, class S>
struct M<State,A,S> {
    static State<A,S> ret( A&& a  ) {
        return State<A,S>( Fn_( [a]( S s ){ return Pair<A,S>( a, s ); }  ) );
    }
};

template <class S, class A>
State<A,S> retStM( A a ) {
    return State<A,S>( Fn_( [a]( S s ){ return Pair<A,S>( a, s ); } ) );
}

///////////////////////////// END MONAD CONCEPT ////////////////////////////////

//////////////////////////// START FUNCTOR CONCEPT /////////////////////////////

#if defined(__GNUG__) && !defined(__clang__)
template <template <class> class F, class A, class B, class S>
    requires Fun<F,A,B>
#else
template <template <class> class F, class A, class B, class S>
#endif
State<B,S> fmap( F<B(A)> f, State<A,S> s ) {
    return State<A,S>(
            Fn_(
                [f,s]( S st ) {
                    auto pair = runState( s, st );
                    return Pair<A,S>( f(pair.first), pair.second );
                }
            )
        );
}

///////////////////////////// END FUNCTOR CONCEPT //////////////////////////////

///////////////////////// START APPLICATIVE CONCEPT ////////////////////////////

// (<*>) :: f (a -> b) -> f a -> f b
template <class S, class A, class B>
State<B,S> seqApp( State<Fn<B(A)>, S> sf, State<A,S> a ) {
    return
        State<B,S> (
            [&sf,&a]( S s ) {
                auto fstRes = sf.runState( s );
                auto f = fstRes.first;
                auto sp = fstRes.second;

                auto secRes = a.runState( sp );
                auto x = secRes.first;
                auto spp = secRes.second;

                return Pair<B,S>( f(x), spp );
            }
        );
}

template <class S, class A, class B>
State<B,S> seqFAct( State<A, S> a, State<B,S> ) {
    return a;
}

template <class S, class A, class B>
State<B,S> seqSAct( State<A, S>, State<B,S> b ) {
    return b;
}

////////////////////////// END APPLICATIVE CONCEPT /////////////////////////////

}

#endif
