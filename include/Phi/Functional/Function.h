/* -*- C++ -*- */

#ifndef FUNCTION
#define FUNCTION

#include <Std/TypeTraits.h>

#include <cassert>
#include <type_traits>
#include <utility>
#include <cstdlib>
#include <typeinfo>
#include <memory>

#include <iostream>
#include <string>

namespace Phi
{

template <typename T> class Fn
{};

#if defined(__clang__)
#elif defined(__GNUG__)

template <class T> concept FunC = std::is_function<T>::value;

template <template <class T> class F, class T, class U>
concept Fun = requires( F<U(T)> fa )
{
    { fa } -> std::convertible_to<Fn<U(T)>>;
};

#else
#endif

/**
 * @class Fn
 *
 * \ingroup Core
 *
 * \brief  First class function.
 *
 * \note Need performance tests.
 *
 * \author Javier Jaramago Fernández
 *
 * \version 0.1
 *
 * Contact: jaramago.fernandez.javier@gmail.com
 *
 */
template <typename Out, typename... In>
class Fn<Out(In...)>
{
public:
    /**
     * \~english @brief Constructor that creates a Fn from a function pointer.
     * \~spanish @brief Contructor que crea una Fn usando un puntero a función.
     *
     * \~english @param fn Fn pointer that is going to be stored for a
     *  later execution.
     * \~spanish @param fn El puntero a función que va a ser guardado para su
     *  ejecución posterior.
     */
    Fn( Out(*fn)( In... ) ) : fn( fn ) {
        this->fnExe = createExecutor( fn );
    }

    template<typename _From, typename _To>
    using CheckFuncReturnType
      = std::disjunction<std::is_void<_To>, std::is_same<_From, _To>, std::is_convertible<_From, _To>>;

    template <class T>
    using FnLmType =
        decltype(( &std::decay<T>::type::operator() ));

    /**
     * \~english @brief Constructor that creates a Fn from a lm.
     *
     * Details:
     *      - Static assertion: The static assert ensures that the parameters
     *        and return types of the lm match with the one provided to
     *        the Fn. Remember that the parenthesis operator is a constant
     *        function for lms, so the equivalence should be done against a
     *        constant pointer.
     *
     * \~spanish @brief Constructor que crea una Fn desde un tipo "Callable".
     *
     * @param lm Lambda that is going to be stored for a later execution.
     * @param lm Tipo callable que va a ser guardado para su ejecución
     *  posterior.
     */
    template <
        class T,
        EnableIfT<
            !IsSameV< RmCVRefT<T>, Fn >
            && !std::is_base_of_v< Fn, RmCVRefT<T> >
            && !std::is_pointer_v<T>
            && std::is_class_v<T>
            && CheckFuncReturnType<std::invoke_result_t<T, In...>, Out>::value,
            bool
       > = true
    >
    Fn( const T& lm ) {
        // static_assert(
        //     std::is_class<T>::value
        //     , "Argument is not a lambda or class type."
        // );
        static_assert(
            std::is_same
            <
              Out(T::*)(In...) const
            , decltype(( static_cast<Out(T::*)(In...) const>( &std::decay<T>::type::operator() ) ))
            >::value
            , "Lambda parameters or return types are different."
        );

        using F = Out(In...);
        clsCtor
        <
          typename std::is_assignable<F*&, decltype(lm)>::type
        , T
        >::build( this, lm );
    }

    /**
     * \~english @brief Copy constructor.
     * \~spanish @brief Constructor copia.
     *
     * \~english @param other Fn to copy.
     * \~spanish @param other Fn a ser copiada.
     */
    Fn( const Fn<Out(In...)>& other ) : fnExe( other.fnExe ) {
        if( other.fn != nullptr ) {
            this->fn = other.fn;
        } else {
            this->lm  = other.lmCpy( other.lm );
            this->lmCpy = other.lmCpy;
            this->lmCln = other.lmCln;
        }
    }

    /**
     * \~english @brief Move constructor.
     * \~spanish @brief Constructor 'move'.
     *
     * \~english @param other Rvalue Fn reference.
     * \~spanish @param other 'Rvalue reference' a un tipo Fn.
     */
    Fn( Fn<Out(In...)>&& other ) : fnExe( other.fnExe ) {
        if( isPFunction( other ) ) {
            this->fn  = other.fn;
            other.fn  = nullptr;
        } else {
            this->lm  = other.lm;
            this->lmCpy = other.lmCpy;
            this->lmCln = other.lmCln;
            other.lm  = nullptr;
            other.lmCpy = nullptr;
            other.lmCln = nullptr;
        }

        other.fnExe = createExecutor( []( In... ) -> Out { std::exit( 1 ); } );
    }

    /**
     * \~english @brief Destructor.
     * \~spanish @brief Destructor.
     */
    ~Fn() {
        if ( this->lmCln != nullptr && this->lm != nullptr ) {
            this->lmCln( this->lm );
        }
    }

    /**
     * \~english @brief Assignment operator.
     * \~spanish @brief Operador de asignación.
     *
     * \~english @param other Constant lvalue Fn reference to be copied.
     * \~spanish @param other Referencia lvalue constante a una Fn a ser copiada.
     */
    Fn<Out(In...)>& operator=( const Fn<Out(In...)>& other ) {
        if ( isPFunction( other ) ) { toPFunction( other ); }
        else { toLFunction( other ); }

        return *this;
    }

    /**
     * \~english @brief Move operator.
     * \~spanish @brief Operador 'Move'.
     *
     * \~english @param other Rvalue Fn reference to be copied.
     * \~spanish @param other 'Rvalue reference' a ser movida.
     */
    Fn<Out(In...)>& operator=( Fn<Out(In...)>&& other ) {
        if ( isPFunction( other ) ) {
            if ( inLFunction() ) {
                this->lmCln( this->lm );

                this->lm  = nullptr;
                this->lmCpy = nullptr;
                this->lmCln = nullptr;
            }

            this->fn = other.fn;
        } else {
            if ( inLFunction() ) { this->lmCln( this->lm ); }
            else { this->fn = nullptr; }

            this->lm  = other.lm;
            this->lmCpy = other.lmCpy;
            this->lmCln = other.lmCln;
        }

        this->fnExe = other.fnExe;

        other.fn  = nullptr;
        other.lm  = nullptr;
        other.lmCpy = nullptr;
        other.lmCln = nullptr;
        other.fnExe = createExecutor( []( In... ) -> Out { std::exit( 1 ); } );

        return *this;
    }

    /**
     * \~english @brief 'Function call' operator.
     * \~spanish @brief Operador 'Function call'.
     *
     * \~english @param in Parameters that are going to be passed to the inner function.
     * \~spanish @param in Parámetros que van a ser pasados a la función contenida.
     */
    inline constexpr Out operator()( const In... in ) const {
        return fnExe( this->lm, this->fn, in... );
    }

private:
    /**
     * @brief Erased pointer in which a lm is going to be stored.
     *
     * Explanation:
     * First we should remember that a lm function is really a "closure",
     * more clearly, it's an anonymous data type that overload his parenthesis
     * operator.
     * Therefore the point of storing it this way, is to to have another lm
     * that is going to be stored as a function pointer, and that is going to
     * be used as executor of this one. That "Fn executor" is going to
     * store the type of the lm we are storing in this pointer,
     * removing the necessity for type Fn of knowing the type of the
     * lm in order to call it.
     */
    void* lm = nullptr;

    /**
     * @brief Fn pointer in which a simple function is going to be stored.
     *
     * Explanation:
     * This pointer is going to store a function. It wont be called directly,
     * like the lm function store in the void pointer, instead the other
     * function, "fnExe" will call it.
     */
    Out (*fn)( In... ) = nullptr;

    /**
     * @brief Fn pointer that will store a lm function
     * specifically generated to execute the type that has been stored.
     *
     * Explanation:
     * This is the "function executor". When the operator of the
     * Fn type is called, the lm function pointed here is executed.
     * It will specifically call the lm function pointer, if the stored type
     * was a lm, or the function pointer, if the stored type was a function.
     */
    Out (*fnExe)( void*, Out(*)( In... ), const In&... ) = nullptr;

    /**
     * @brief Container for a lm function that erases the lm stored when
     * destructor is called.
     *
     * Explanation:
     * The point for needing a lm function to simply delete the stored
     * lm, is that it is stored as a void pointer. So the Fn type
     * doesn't know which is the type of that stored lm. The solution
     * is that the lmCln that is created in the constructor which
     * receives a lm as parameter will hold that type.
     */
    void (*lmCln)( void* ) = nullptr;

    /**
     * @brief Container to store the lm function that is used when the
     * type is copied.
     *
     * Explanation:
     * As for the lmCln is necessary to have a lm function because
     * we need to store the type of the incoming lm in order to copy it
     * later.
     */
    void* (*lmCpy)( void* ) = nullptr;

    /**
     * @brief Change the state of this function to match a Fn constructed from
     * a function pointer.
     *
     * @param other The other function which state should be copied into this
     * one.
     *
     * @return Nothing.
     */
    auto toPFunction( const Fn<Out(In...)>& other ) {
        if( inLFunction() ) {
            this->lmCln( this->lm );
            this->fnExe = other.fnExe;
            this->lm  = nullptr;
            this->lmCpy = nullptr;
            this->lmCln = nullptr;
        }

        this->fn = other.fn;
    }

    /**
     * @brief Convert this function to the LFuction passed as parameter.
     *
     * @param other LFunction in which you want to be convert this function.
     *
     * @return Nothing.
     */
    auto toLFunction( const Fn<Out(In...)>& other ) {
        if( inPFunction() ) {
            this->fn  = nullptr;
            this->fnExe = other.fnExe;
        } else {
            this->lmCln( this->lm );
        }

        this->fnExe = other.fnExe;
        this->lm  = other.lmCpy( other.lm );
        this->lmCpy = other.lmCpy;
        this->lmCln = other.lmCln;
    }

    /**
     * @brief Convert this frunction to the DFunction passed as parameter.
     *
     * @param other Dfunction in which you want to convert this function.
     *
     * @return Nothing.
     */
    auto toDFunction( const Fn<Out(In...)>& other ) {
        if( inPFunction() ) {
            this->fn  = nullptr;
            this->fnExe = other.fnExe;
        } else {
            this->lmCln( this->lm );
            this->fnExe = other.fnExe;
            this->lm  = nullptr;
            this->lmCpy = nullptr;
            this->lmCln = nullptr;
        }
    }

    /**
     * \~english @brief
     *  Check if this function is a PFunction.
     * \~spanish @brief
     *  Comprueba si esta función es una PFunction.
     *
     * \~english @return true if this a PFunction.
     * \~spanish @return true Si esta función es una PFunction.
     */
    bool inPFunction() {
        if( this->fn != nullptr ) { return true; }
        else { return false; }
    }

    /**
     * \~english @brief
     *  Check if this function is a LFunction.
     * \~spanish @brief
     *  Comprueba si esta función es una LFunction.
     *
     * \~english @return true if this is a LFunction.
     * \~spanish @return true si esta función es una LFunction.
     */
    bool inLFunction() {
        if( this->lm != nullptr ) { return true; }
        else { return false; }
    }

    /**
     * \~english @brief
     *  Check if the function received as parameter is a PFunction.
     * \~spanish @brief
     *  Comprueba si la función recibida como parámetro es una PFunction.
     *
     * \~english @param other The function to be checked.
     * \~spanish @param other La función a ser comprobada.
     *
     * \~english @return true if the received function is a PFunction.
     * \~spanish @return true si la función recibida es una PFunction.
     */
    bool isPFunction( const Fn<Out(In...)>& other ) {
        if( other.fn != nullptr ) { return true; }
        else { return false; }
    }

    /**
     * \~english @brief
     *  Check if the function received as parameter is a LFunction.
     * \~spanish @brief
     *  Comprueba si la función recibida como parámetro es una LFunction.
     *
     * \~english @param other The function to be checked.
     * \~spanish @param other La función a ser comprobada.
     *
     * \~english @return true if the received function is a LFunction.
     * \~spanish @return true si la función recibida es una LFunction.
     */
    bool isLFunction( const Fn<Out(In...)>& other ) {
        if( other.lm != nullptr ) { return true; }
        else { return false; }
    }

    template <class F,class T>
    struct clsCtor {};

    template <class T>
    struct clsCtor<std::true_type, T>
    {
        static auto build( Fn<Out(In...)>* fun, const T& lm ) {
            fun->fn = lm;
            fun->fnExe = fun->createExecutor( static_cast<Out(*)(In...)>( lm ) );
        }
    };

    template <class T>
    struct clsCtor<std::false_type, T>
    {
        static auto build( Fn<Out(In...)>* fun, const T& lam ) {
#ifndef __clang_analyzer__
            fun->lm = new T( lam );
#endif
            fun->fnExe = fun->createExecutor( lam );
            fun->lmCln = []( void* lm ) { delete static_cast<T*>(lm); };
            fun->lmCpy = []( void* lm ) -> void* { return new T( *( static_cast<T*>(lm) ) ); };
        }
    };

    /**
     * \~english @brief
     *  Fn that creates a lm that typecast his first parameter
     *  to the type that this function receives.
     * \~spanish @brief
     *  Función que crea una lambda a que ejecuta el operador el 'operador de
     *  llamada' sobrecargado del tipo que recibe.
     */
    template <typename T>
    auto createExecutor( T const & ) {
        return []( void *lm, Out(*)( In... ), const In&... arguments ) -> Out {
            return static_cast<T*>(lm)->operator()( std::forward<In>(const_cast<In&>(arguments))... );
        };
    }

    /**
     * \~english @brief
     *  Fn that creates a lm that execute the function pointer
     *  that it receives receives.
     * \~spanish @brief
     *  Función que crea una lambda a que ejecuta el puntero a función
     *  que recibe.
     */
    auto createExecutor( Out(*)( In... ) ) {
        return []( void *, Out(*fn)( In... ), const In&... arguments ) -> Out {
            return (*fn)( std::forward<In>(const_cast<In&>(arguments))... );
        };
    }
};

/**
 * \~english @brief Fn composition operator.
 *
 *  Explanation:
 *  Creates a lm that encapsulate two functions and return it. The second
 *  function should returns the type that the first one accept as argument.
 *
 * \~spanish @brief Operador de composición de Fns.
 *
 *  Explicación:
 *  Crea una lambda que encapsula las dos funciones y las devuelve. La
 *  segunda función debería devolver el tipo que la primera toma como
 *  argumento.
 */
template<typename T, typename R, typename... Args>
Fn<R(Args...)> operator*( const Fn<R(T)> f1, const Fn<T(Args...)> f2 ) {
    return ([=]( Args... args ) { return f1( f2( args... ) ); });
}

/**
 * \~english @brief
 * Helper type for constructing any function with the same function
 * call.
 *
 * The problem:
 * Our function constructor can create a function from a pointer and from a
 * lm, but while trying to create a function from a lm, the type should
 * be fully specified. That implies that you should already have a constructed
 * type in which you can store the lm through assignation, or you can call
 * the constructor with the type being fully specified. This can be a bit
 * tedious and boilerplate.
 *
 * Solution:
 * First we create one type with a single template parameter, and a second type
 * with three type parameters( described below ), being the last type paremeter
 * a variadic template argument. Which are the minimum to represent a generic
 * pointer to a member function.
 * First type will be a derived class of the second one, that way it can
 * pass to its parent class the result of calling the "decltype" operator over
 * the received template paremeter ( the lm type ).
 *
 * \~spanish @brief
 * Tipo ayudante para construir cualquier función 'Fn' usando la misma
 * llamada a función.
 */
template <class T>
struct creator : creator<decltype(&T::operator())> {};

/**
 * \~english @brief
 *  Helper type for constructing any function with the same function
 *  call.
 *
 *  Solution:
 *  Now in this type you need to overload the parenthesis operator. It needs to
 *  be a templated function taking a generic argument and returning a Fn with the
 *  types the class receive as template arguments.
 *
 * \~spanish @brief
 *  Tipo ayudante para construir cualquier función con la misma llamada a
 *  función.
 *
 *  Solución:
 *  En este tipo, necesitamos hacer overloading del 'call operator'. Necesitamos que
 *  sea una 'template function' tomando un argumento genérico, y devolviendo
 *  una 'Fn' parametrizada con los argumentos apropiados.
 */
template <class clasn, class ret, class... args>
struct creator<ret(clasn::*)(args...) const> {
    template <class T>
    Fn<ret(args...)> operator()( T const& lm ) {
        return Fn<ret(args...)>( lm );
    }
};

/**
 * \~english @brief
 *  Function to construct a Fn from a lm.
 * \~spanish @brief
 *  Función para construir una Fn usando un tipo 'callable'.
 *
 * \~english @param lm
 *  Lambda function that will be passed as argument to the function
 *  constructor.
 * \~spanish @param lm
 *  Tipo 'callable' a ser encapsulado.
 */
template <class T>
inline constexpr auto Fn_( T const& lm ) {
    return creator<T>()( lm );
}

/**
 * \~english @brief
 *  Function to construct a Fn from a function pointer.
 * \~spanish @brief
 *  Función para construir una Fn desde un puntero a función.
 * \~english @param f
 *  Function pointer that will be passed as argument to the function
 *  constructor.
 * \~spanish @param f
 *  Puntero a función.
 */
template <class Out, class... In>
inline constexpr auto Fn_( Out(*f)(In...) ) {
    return Fn<Out(In...)>( f );
}

/**
 * @brief Helper type to extract the correct arguments types from the
 * arguments parameter pack.
 *
 * Used as a wrapper to capture the "whole" pack.
 */
template <class... Args>
struct Extract {};

/**
 * @brief Helper type to extract the correct arguments types from the
 * arguments parameter pack.
 */
template <std::size_t I, class... Ts>
struct ExtractF {};

template <std::size_t I, class T, class... Ts>
struct ExtractF<I, T, Ts...> : ExtractF<I-1, Ts...> {};

template <class T, class... Ts>
struct ExtractF<0, T, Ts...> : Extract<T,Ts...> {};

template <>
struct ExtractF<0>: Extract<>{};

template <class... args>
struct Pack {};

template <class T> class Fp
{};

template <class Out, class... In>
inline constexpr auto Fp_( Fn<Out(In...)> fn )
{
    return Fp<Out(In...)>( fn );
}

using std::forward;
using std::move;
using std::make_tuple;
using std::tuple_cat;
using std::apply;

template <class P1, class P2, bool = std::is_same<P1,P2>::value>
struct Match : std::true_type {};

template <class P1, class P2>
struct Match<P1, P2, true> {
    template <class... Args3, class F, class... Args>
    inline auto operator()( Extract<Args3...>, F&& fn, Args&&... args ) {
        // This should be reached at the end of recursion, this is
        // just a guard checking if the condition is met before it should.)
        static_assert(sizeof...(Args3) == 0);

        return fn( forward<Args>( args )... );
    }
};

template <class P1, class P2>
struct Match<P1, P2, false> : std::false_type {
    template <class... Args3, class F, class... Args1>
    inline auto operator()( Extract<Args3...>, F fn, Args1&&... args1 ) {
        return Fp_( Fn_(
           [fn, args = make_tuple(forward<Args1>(args1)...)]
           ( Args3&&... args3 ) -> auto {
               return apply(
                    fn,
                    tuple_cat(
                       move(args),
                       make_tuple(forward<Args3>(args3)...)
                    )
                );
           }
        ) );
    }
};

std::string x_demangle(const char* mangled);

template <class Out, class... In>
class Fp<Out(In...)>
{
public:
    Fp( Fn<Out(In...)> fn ) : fn( fn ) {}

    template <class... In_>
    inline auto operator()( In_&&... a )
    {
        Match< Pack<In_&&...>, Pack<In&&...> > m;

        return m( ExtractF<sizeof...(In_), In...>()
                , std::forward<Fn<Out(In...)>>( fn )
                , std::forward<In_>( a )... );
    }

private:
    Fp() = delete;
    Fn<Out(In...)> fn;
};

// return Match< Pack<In_...>, Pack<In...> >::value;
// return x_demangle(typeid(m).name());//( fn, a... );
template<typename T, typename... Rest>
struct is_conv : std::false_type {};

template<typename T, typename First>
struct is_conv<T, First> : std::is_convertible<First, T> {};

/**
 * \~english @brief
 * Recursive structure definition for making sure that
 * the all the variant types supplied match with the first
 * supplied one.
 * \~spanish @details
 * Estructura definida recursivamente para asegurar que todas
 * las alternativas del variante proporcionadas son convertibles
 * a la primera.
 */
template<typename T, typename First, typename... Rest>
struct is_conv<T, First, Rest...>
    : std::integral_constant<
        bool,
        std::is_convertible<First, T>::value &&
        is_conv<T, Rest...>::value> {};

/**
 * @struct BFn
 *
 * \~english @brief
 * Base Function experimental type, not used yet.
 * \~spanish @brief
 * Tipo experimental, no usado aun.
 */
template <class T>
struct BFn {};

template <class T>
struct EFnC {};

template <template <class> class F, class R, class... Args>
struct EFnC<F<R(Args...)>> : BFn<R> {
};


/**
 * @struct EFn
 *
 * \~english @brief
 * Recursive function definition using operator overloading for
 * type matching.
 * \~spanish @brief
 * Función definida recursivamente usando sobrecarga de operadores
 * para hacer el matching de tipos.
 */
template < class... Fns>
struct EFn : Fns... {
    EFn( Fns&&... fns ) : Fns(std::forward<Fns>(fns))...
    {}

    using Fns::operator()...;
};

/**
 * \~english @brief
 * C++17 guide deductions is need to forward the types used in
 * the constructor of EFn.
 * \~spanish @brief
 * C++17 'deduction guides' son necesarias para hacer un forwarding
 * de los tipos en la construcción de EFn.
 */
template <class... A>
EFn(A...) -> EFn<std::decay_t<A>...>;

/**
 * \~english @brief
 * Helper function for building Efn.
 * \~spanish @brief
 * Función de conveniencia para construir EFn.
 */
template <class R, class FnF, class... Fns>
EFn<FnF,Fns...> EFn_(FnF&& fnf, Fns&&... fns) {
    static_assert(
       is_conv<
           BFn<R>,
           EFnC<FnF>,
           EFnC<Fns>...
       >::value,
       "Functions should return the same type"
   );

    return EFn(std::forward<FnF>(fnf), std::forward<Fns>(fns)...);
}

}

#endif // FUNCTION
