/* -*- C++ -*- */

#ifndef PHI_BOOL_H
#define PHI_BOOL_H

namespace Phi
{

/////////////////////////// START TYPE DEFINITION //////////////////////////////

struct Bool
{
    virtual operator bool() const { return _val; }
    explicit Bool(bool val) { this->_val = val;}
    friend Bool True();
    friend Bool False();
private:
    bool _val;
};

Bool True();
Bool False();

//////////////////////////// END TYPE DEFINITION ///////////////////////////////

}

#endif
